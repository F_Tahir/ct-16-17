package gen;

import ast.*;
import sem.TypeCheckVisitor;

import java.io.PrintWriter;

public class StringVisitor implements ASTVisitor<Void> {

    private PrintWriter writer;

    // Used to label the strings in .data. Also stored with the object.
    private int labelCount = 1;

    public StringVisitor(PrintWriter writer, Program program) {
        this.writer = writer;
        visitProgram(program);
    }


    @Override
    public Void visitBaseType(BaseType bt) {
        return null;
    }

    @Override
    public Void visitStructType(StructType st) {
        return null;
    }

    @Override
    public Void visitBlock(Block b) {
        for (Stmt s : b.stmts) {
            s.accept(this);
        }
        return null;
    }

    @Override
    public Void visitFunDecl(FunDecl p) {
        p.block.accept(this);
        return null;
    }

    @Override
    public Void visitProgram(Program p) {


        for (StructType st : p.structTypes) {
            st.accept(this);
        }

        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
        }

        for (FunDecl fd : p.funDecls) {
            fd.accept(this);
        }
        return null;
    }

    @Override
    public Void visitVarDecl(VarDecl vd) {
        return null;
    }

    @Override
    public Void visitVarExpr(VarExpr v) {
        v.vd.accept(this);
        return null;
    }

    @Override
    public Void visitReturn(Return rt) {
        rt.expr.accept(this);
        return null;
    }

    @Override
    public Void visitAssign(Assign assign) {
        assign.expr1.accept(this);
        assign.expr2.accept(this);
        return null;
    }

    @Override
    public Void visitIf(If if1) {
        if1.expr.accept(this);
        if1.stmt1.accept(this);

        if (if1.stmt2 != null) {
            if1.stmt2.accept(this);
        }
        return null;
    }

    @Override
    public Void visitWhile(While while1) {
        while1.expr.accept(this);
        while1.stmt.accept(this);
        return null;
    }

    @Override
    public Void visitExprStmt(ExprStmt exprStmt) {
        exprStmt.expr.accept(this);
        return null;
    }

    @Override
    public Void visitTypecastExpr(TypecastExpr typecastExpr) {
        typecastExpr.expr.accept(this);
        typecastExpr.type.accept(this);
        return null;
    }

    @Override
    public Void visitSizeOfExpr(SizeOfExpr sizeOfExpr) {
        sizeOfExpr.type.accept(this);
        return null;
    }

    @Override
    public Void visitValueAtExpr(ValueAtExpr valueAtExpr) {
        valueAtExpr.expr.accept(this);
        return null;
    }

    @Override
    public Void visitFieldAccessExpr(FieldAccessExpr fieldAccessExpr) {
        fieldAccessExpr.expr.accept(this);
        return null;
    }

    @Override
    public Void visitArrayAccessExpr(ArrayAccessExpr arrayAccessExpr) {
        arrayAccessExpr.expr1.accept(this);
        arrayAccessExpr.expr2.accept(this);
        return null;
    }

    @Override
    public Void visitBinOp(BinOp binOp) {
        binOp.expr1.accept(this);
        binOp.expr2.accept(this);
        return null;
    }

    @Override
    public Void visitFunCallExpr(FunCallExpr funCallExpr) {

        for (Expr e : funCallExpr.expr) {
            e.accept(this);
        }

        return null;
    }

    @Override
    public Void visitIntLiteral(IntLiteral intLiteral) {
        return null;
    }

    @Override
    public Void visitChrLiteral(ChrLiteral chrLiteral) {
        return null;
    }

    @Override
    public Void visitStrLiteral(StrLiteral strLiteral) {
        strLiteral.label = "label_" + labelCount++;
        writer.printf("\t%s: .asciiz \"%s\"\n", strLiteral.label, strLiteral.string);
        return null;
    }

    @Override
    public Void visitArrayType(ArrayType arrayType) {
        return null;
    }

    @Override
    public Void visitPointerType(PointerType pointerType) {
        return null;
    }
}
