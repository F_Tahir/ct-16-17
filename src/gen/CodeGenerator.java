package gen;

import ast.*;
import sem.TypeCheckVisitor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.EmptyStackException;
import java.util.List;
import java.util.Stack;



// TODO: Array access (arrayAccessExpr, pointer access (valueAtExpr), function calls (params, returns also).
// TODO: Check sizes (make sure everything is aligned correctly etc), also check visitFieldAccessExpr.
public class CodeGenerator implements ASTVisitor<Register> {

    /*
     * Simple register allocator.
     */

    // contains all the free temporary registers
    private Stack<Register> freeRegs = new Stack<Register>();

    public CodeGenerator() {
        freeRegs.addAll(Register.tmpRegs);
    }

    private class RegisterAllocationError extends Error {
    }

    private Register getRegister() {
        try {
            return freeRegs.pop();
        } catch (EmptyStackException ese) {
            throw new RegisterAllocationError(); // no more free registers, bad luck!
        }
    }

    private void freeRegister(Register reg) {
        freeRegs.push(reg);
    }


    public PrintWriter writer; // use this writer to output the assembly instructions
    private Program program; // Set this in emitProgram to be used for string traversal

    // Global variable used to fill offset field of VarDecl
    private int offset = 0;

    // Need to do separate things when accepting lhs and rhs in assign,
    // using a variable is an alternative to Prof. Dubach's 2 pass suggestion
    // (although doesn't use OO principles).
    private boolean acceptingLhs = false;

    // Used to determine whether an array access is being carried out. If it is,
    // then don't want to load word in VarExpr - want to increment address first.
    private boolean arrayAccessLhs = false;

    // Need this flag to load word instead of address in VarExpr, if the index is
    // a variable.
    private boolean arrayAccessRhs = false;

    // Global variable to calculate space of structs and arrays
    private int space = 0;

    // Counter used to create unique labels, starting with L1
    private int labelCount = 0;

    // Used in VisitFieldAccess, VisitArrayAccess and VisitValueAtExpr, set in VisitVarExpr
    private VarDecl globalVarDecl;


    // If true, need to set offsets for func params starting from 0
    private boolean acceptingFuncParams = false;





    public void emitProgram(Program program, File outputFile) throws FileNotFoundException {
        writer = new PrintWriter(outputFile);

        // Traverse the AST once to store strings in .data segment
        this.program = program;
        visitProgram(program);
        writer.close();
    }


    // If we have ArrayType(INT, 15)..., this method would return BaseType.INT
    public Type getLowerType(Type t) {
        if (t instanceof BaseType) {
            return t;
        } else if (t instanceof ArrayType) {
            return ((ArrayType) t).type;
        } else if (t instanceof PointerType) {
            return ((PointerType) t).type;
        }
        return null;
    }

    // If we have nested, e.g. ArrayType(PointerType(INT...), returns INT by recursing through
    public Type getBaseType(Type t) {

        while (true) {
            if (t == BaseType.INT || t == BaseType.VOID || t == BaseType.CHAR || t == null) {
                break;
            }

            t = getLowerType(t);
        }
        return t;
    }


    // This function is used to calculate the space required for arrays and structs.
    // TODO: Possibly change ALL character arrays to be 4 bytes for each index, even non-struct arrays.
    // Currently, chars are 1 byte per index in non structs, and 4 bytes in structs (to align address)
    // NOTE: Reset space to 0 after every time this function is called (unless called recursively).
    private int calculateSpace(VarDecl vd) {
        space = 0;
        if (vd.type instanceof ArrayType) {

            // Array of structs, so get struct size and multiply by array index
            if (getLowerType(vd.type) instanceof StructType) {
                String structName = ((StructType)getLowerType(vd.type)).string;
                int structSize = calcSpaceForStruct(structName);
                space = 0;
                return structSize * ((ArrayType) vd.type).int1;
            }

            // All other arrays take 4 bytes per index, even chars
            else {
                return ((ArrayType) vd.type).int1*4;
            }
        }

        // Passed in argument is of struct type, so check how much data the struct requires
        if (vd.type instanceof StructType) {
            String structName = ((StructType) vd.type).string;
            List<VarDecl> structVars = TypeCheckVisitor.structVarData.get(structName);

            // Loop through each struct's variable and calculate the space
            for (VarDecl vd1 : structVars) {

                // If it's an array, check whether it's an array of pointers,
                // array of ints or array of chars
                if (vd1.type instanceof ArrayType) {
                    // All arrays in a struct require 4 bytes for each index
                    space += ((ArrayType) vd1.type).int1*4;

                    // Struct field is a struct type, so may be more than 4 bytes
                } else if (vd1.type instanceof StructType) {
                    List<VarDecl> structFieldVars = TypeCheckVisitor.structVarData.get(
                        ((StructType) vd1.type).string);

                    for (VarDecl vd2 : structFieldVars) {
                        space += calculateSpace(vd2);
                    }

                } else {
                    space += 4;
                }
            }
            // Can be a pointertype, int, or char which should all use 4 bytes
        } else {
            return 4;
        }
        System.out.println(space);
        return space;
    }

    // Used in sizeof to calculate how much space a struct decl requires
    private int calcSpaceForStruct(String name) {
        int sizeOfSpace = 0;
        List<VarDecl> listOfVars = TypeCheckVisitor.structVarData.get(name);

        for (VarDecl vd : listOfVars) {
            sizeOfSpace += calculateSpace(vd);
            space = 0;
        }

        // Reset global space to 0, as this is changed when calling calculateSpace
        space = 0;
        return sizeOfSpace;
    }

    /*
    This function is used to calculate the offset value to access a structure field in the stack.
    It recurses through the structs variables, calculates the size and adds them up. This is done
    until it encounters the variable being called, and we then know the offset from there.
     */
    private int calcOffsetForFieldAccess(String name, String fieldName) {

        List<VarDecl> listOfVars = TypeCheckVisitor.structVarData.get(name);
        int result = 0;
        space = 0;

        for (VarDecl vd1 : listOfVars) {

            if (fieldName.equals(vd1.varName)) {
                break;
            }
            result += calculateSpace(vd1);
            space  = 0;
        }

        return result;
    }


    @Override
    public Register visitBaseType(BaseType bt) {
        return null;
    }

    @Override
    public Register visitStructType(StructType st) {
        return null;
    }

    @Override
    public Register visitBlock(Block b) {
        for (VarDecl vd : b.params) {
            vd.accept(this);
        }

        for (Stmt s : b.stmts) {

            // Bug fix to allow nested control flow.
            // Kind of hacky but I think it works.
            if (s instanceof While || s instanceof If) {
                labelCount += 1;
            }
            s.accept(this);
        }
        return null;
    }

    @Override
    public Register visitFunDecl(FunDecl p) {
        writer.printf("%s:\n", p.name);

        acceptingFuncParams = true;
        offset = 0;

        for (VarDecl vd : p.params) {
            vd.accept(this);
        }


        acceptingFuncParams = false;

        writer.printf("\tsw %s, %d(%s)\n", Register.ra, (p.params.size())*4, Register.sp);

        p.block.accept(this);
        return null;
    }

    @Override
    public Register visitProgram(Program p) {

        writer.printf("\t.data\n\n");

        for (StructType st : p.structTypes) {
            visitStructType(st);
        }

        for (VarDecl vd : p.varDecls) {
            visitVarDecl(vd);
        }

        // Traverse the tree for strings AFTER accepting VarDecl's, to avoid unaligned addresses!
        new StringVisitor(writer, program);


        writer.print("\n\n\t.text\n\n");

        // Ensure that main is the first label generate
        for (FunDecl fd : p.funDecls) {
            if (fd.name.equals("main")) {
                fd.accept(this);
                writer.printf("\tj exit\n\n");
            }
        }

        // Ensure that main is the first label generate
        for (FunDecl fd : p.funDecls) {
            if (!fd.name.equals("main")) {
                fd.accept(this);
                writer.printf("\tjr $ra\n");
            }
        }


        writer.printf("\n\nexit:\n");
        writer.printf("\t# Exiting Program\n");
        writer.printf("\tli %s 10\n", Register.v0.toString());
        writer.printf("\tsyscall\n");


        writer.flush(); // Save and close
        return null;
    }

    @Override
    public Register visitVarDecl(VarDecl vd) {

        // The VarDecl was defined in global scope
        if (vd.isGlobalVar) {

            if (vd.type == BaseType.INT || vd.type == BaseType.CHAR) {
                // Int is 4 bytes (i.e. one word).
                // Char is also 4 bytes (design choice, normally just 1 byte)
                writer.printf("\t%s: .word 0\n", vd.varName);


                // A pointer is an address, which is simply an integer (4 bytes),
                // therefore store it as a .word
            } else if (vd.type instanceof PointerType) {

                writer.printf("\t%s: .word 0\n",
                        vd.varName);

            } else if (vd.type instanceof ArrayType) {
                writer.printf("\t%s: .space %d \t\n",
                        vd.varName, calculateSpace(vd));

            } else if (vd.type instanceof StructType) {
                String structName = ((StructType) vd.type).string;
                List<VarDecl> structVars = TypeCheckVisitor.structVarData.get(structName);

                // Accept each varDecl so code can be emitted in the .data segment
                // Need to append the struct name to the label
                for (VarDecl vd2 : structVars) {
                    writer.printf("\t%s_%s_%s: .space %d\n",
                            ((StructType) vd.type).string, vd.varName, vd2.varName,
                            calculateSpace(vd2));
                    space = 0;
                }

            }
        }

        else if (acceptingFuncParams) {
            vd.offset = offset;
            offset += 4;
        }

        // The varDecl is coming from a function (could be params so may need to separate params too)
        else {

            // Local array declarations. Each index needs 4 bytes, regardless of it being int or char
            if (vd.type instanceof ArrayType) {
                int index = ((ArrayType) vd.type).int1 * 4;
                vd.offset = offset;
                offset += index;
                writer.printf("\taddi %s, %s, -%d\n",
                        Register.sp.toString(), Register.sp.toString(), index);

                // Local struct variable, so call calculateSpace() to see how much
                // space it needs, and move the stack pointer.
            } else if (vd.type instanceof StructType) {
                int offsetVal = calculateSpace(vd);
                vd.offset = offset;
                offset += offsetVal;
                writer.printf("\taddi %s, %s, -%d\n",
                        Register.sp.toString(), Register.sp.toString(), offsetVal);

            } else {
                writer.printf("\taddi %s, %s, -4\n", Register.sp.toString(), Register.sp.toString());
                vd.offset = offset;
                offset += 4;
            }
        }
        return null;
    }


    @Override
    public Register visitVarExpr(VarExpr v) {

        globalVarDecl = v.vd;

        // Global variable, so need to load from .data segment
        if (v.vd.isGlobalVar) {
            Register result = getRegister();
            Register addrReg = getRegister();

            // If the varExpr being accepted is from lhs, then
            // we should only load the address, not the word.
            if (acceptingLhs) {
                writer.printf("\tla %s, %s\n", addrReg.toString(), v.name);

                // RHS of array (the index) is a VarExpr, so we want the value,
                // so we can multiply by 4 abd add to the array address.
                if (arrayAccessRhs) {
                    writer.printf("\tlw %s, (%s)\n", result, addrReg);
                    freeRegister(addrReg);
                    return result;
                }

                // Else return the address only
                freeRegister(result);
                return addrReg;

            } else {

                // We always load the address, regardless of it being an array
                writer.printf("\tla %s, %s\n", addrReg.toString(), v.name);

                // If array access, want to increment addrReg, THEN load word, so don't emit this
                // instruction, do it in VisitArrayAccessExpr instead.
                if (!arrayAccessLhs) {
                    System.out.println("Here for " + v.name);
                    writer.printf("\tlw %s, (%s)\n", result.toString(), addrReg.toString());
                    freeRegister(addrReg);
                    return result;
                }

                // Only reach this if the expression was an array access attempt
                freeRegister(result);
                return addrReg;
            }


            // Local variable, need to offset the stack pointer
        } else {
            Register result = getRegister();
            Register addrReg = getRegister();


            // May need to surround this with if (acceptingLhs)
            if (acceptingLhs) {
                writer.printf("\tla %s, %d(%s)\n", result.toString(), v.vd.offset, Register.sp.toString(),
                        v.vd.offset);
                freeRegister(addrReg);
                return result;

                // If there's a VarExpr in index, then load the value. Or if we're
                // not accepting lhs, and it's not an array, then load the value.
                // If it's an array, we need to increment the address before loading
                // from it.
            } else {
                writer.printf("\tlw %s, %d(%s)\n", addrReg, v.vd.offset, Register.sp);
                freeRegister(result);
                return addrReg;
            }

        }
    }

    @Override
    public Register visitReturn(Return rt) {
        Register returnReg = rt.expr.accept(this);
        Register result = getRegister();


        writer.printf("\tmove %s, %s\n", result, returnReg);
        writer.printf("\tjr %s\n", Register.ra);

        freeRegister(returnReg);

        // Load back ra
        writer.printf("\tlw %s 0(%s)\n", Register.ra, Register.sp);

        // Reset sp
        writer.printf("\tadd %s, %s, $zero\n", Register.sp, Register.fp);


        // Jump to ra.
        writer.printf("\tjr %s\n", Register.ra);

        return result;
    }



    @Override
    // TODO: Assign pointers, and array access. Struct access should be done, need to test.
    public Register visitAssign(Assign assign) {

        // Accept rhs first.
        Register rhs = assign.expr2.accept(this);

        // We don't want to lw in visitVarExpr, only la, so this boolean ensures
        // a lw instruction doesn't occur
        acceptingLhs = true;
        Register lhs = assign.expr1.accept(this);
        acceptingLhs = false;

        writer.printf("\tsw %s, (%s)\n", rhs.toString(),  lhs.toString());
        freeRegister(rhs);
        freeRegister(lhs);
        return null;
    }

    @Override
    public Register visitIf(If if1) {

        // Label number to jump into else code if comparison fails
        int currentCount = labelCount;

        // Emits comparison instruction which returns 1 or 0.
        Register exprReg = if1.expr.accept(this);

        // If exprReg result is 0, then comparison is wrong, so jump over the if.
        writer.printf("\tbeqz %s, L_%d\n", exprReg.toString(), labelCount);

        // Emits "if" instructions
        Register stmt1Reg = if1.stmt1.accept(this);
        Register stmt2Reg = null;

        // Label number to jump over else code (in order to skip it if "if" fails)
        int elseLabel = ++labelCount;

        // Emit jump instruction to jump over else only if an else exists
        if (if1.stmt2 != null) {
            writer.printf("\tj L_%d\n", elseLabel);
        }

        // Instructions for else. Jump to this label using bne, if comparison fails
        writer.printf("\n\nL_%d:\n", currentCount);
        if (if1.stmt2 != null) {

            // Hacky bug fix to allow nested (and else if's) to create non-duplicate
            // labels
            labelCount++;
            stmt2Reg = if1.stmt2.accept(this);

            // Used in conjunction with the jump instruction above. If stmt2 is non-null, we need to ensure
            // we can jump over the else stmt, if the comparison in the "if" is true. So if this is the case,
            // we jump to this label to avoid carrying out the else instructions.
            writer.printf("\n\nL_%d:\n", elseLabel);

        }

        freeRegister(exprReg);

        if (stmt1Reg != null)
            freeRegister(stmt1Reg);
        if (stmt2Reg != null)
            freeRegister(stmt2Reg);

        return null;
    }

    @Override
    public Register visitWhile(While while1) {

        int whileLabel = labelCount;
        int currentCount = ++labelCount;

        // Label used to jump back to while statement
        writer.printf("\n\nL_%d:\n", whileLabel);


        // Emits comparison and bne instruction
        Register exprReg = while1.expr.accept(this);

        // If exprReg result is 0, then comparison is wrong, so jump over the if.
        writer.printf("\tbeqz %s, L_%d\n", exprReg.toString(), labelCount);

        // Emits while body instructions
        Register stmtReg = while1.stmt.accept(this);

        // Jump back to while statement and compare again
        writer.printf("\tj L_%d\n\n\n", whileLabel);

        // Label to jump to if while comparison fails (bne in visitBinOp takes care of this jump)
        writer.printf("L_%d:\n", currentCount);

        if (exprReg != null)
            freeRegister(exprReg);
        if (stmtReg != null)
            freeRegister(stmtReg);

        return null;
    }

    @Override
    public Register visitExprStmt(ExprStmt exprStmt) {
        exprStmt.expr.accept(this);
        return null;
    }

    @Override
    public Register visitTypecastExpr(TypecastExpr typecastExpr) {
        Register reg = typecastExpr.expr.accept(this);
        return reg;
    }

    @Override
    public Register visitSizeOfExpr(SizeOfExpr sizeOfExpr) {

        Register result = getRegister();

        if (sizeOfExpr.type == BaseType.INT) {
            writer.printf("\tli %s, %d\n", result.toString(), 4);

        } else if (sizeOfExpr.type == BaseType.CHAR) {
            writer.printf("\tli %s, %d\n", result.toString(), 4);

            // A pointer is simply an int, so size is 4.
        } else if (sizeOfExpr.type instanceof PointerType) {
            writer.printf("\tli %s, %d\n", result.toString(), 4);

            // For struct, call an external function to calculate size.
            // This function also checks for nested structs as well as arrays etc.
        } else if (sizeOfExpr.type instanceof StructType) {


            int space = calcSpaceForStruct(((StructType) sizeOfExpr.type).string);
            writer.printf("\tli %s, %d\n", result.toString(), space);
        }
        return result;
    }

    @Override
    public Register visitValueAtExpr(ValueAtExpr valueAtExpr) {
        // TODO: Implement this
        return null;
    }

    // TODO: CHeck whether the way I am accessing isGlobalVar and vd.offset is valid.
    // TODO: Make nested structs work.
    @Override
    public Register visitFieldAccessExpr(FieldAccessExpr fieldAccessExpr) {

        String structName = ((StructType) fieldAccessExpr.expr.type).string;
        boolean isGlobal = ((VarExpr) fieldAccessExpr.expr).vd.isGlobalVar;

        // Offset only used for local, otherwise we get the label.
        int offset = ((VarExpr) fieldAccessExpr.expr).vd.offset;

        if (isGlobal) {

            Register result = getRegister();
            Register addrReg = getRegister();

            // Label is used for globally declared structs only.
            String label = String.format("%s_%s_%s", structName,
                    ((VarExpr) fieldAccessExpr.expr).vd.varName,
                    fieldAccessExpr.string);

            // Global variable and accepting lhs, so we load address.
            if (acceptingLhs) {
                freeRegister(result);
                writer.printf("\tla %s, %s\n", addrReg.toString(), label);
                return addrReg;

                // Global variable and accepting rhs (so we need to access the value), so load address and then
                // load word from that address.
            } else {
                writer.printf("\tla %s, %s\n", addrReg.toString(), label);
                writer.printf("\tlw %s, (%s)\n", result.toString(), addrReg.toString() );
                freeRegister(addrReg);
                return result;
            }


            // Local variable, so we need an address to load from. If it's a rhs expression, we also need
            // to load the actual data into a register.
        } else {
            Register result = getRegister();
            Register addrReg = getRegister();

            int finalOffset = calcOffsetForFieldAccess(structName, fieldAccessExpr.string);

            // Should be fp - (offset + x).
            writer.printf("\tsubi %s, %s, %d\n", result.toString(), Register.fp.toString(),
                    (offset+finalOffset));

            if (!acceptingLhs) {
                writer.printf("\tlw %s, (%s)\n", addrReg.toString(), result.toString());
                freeRegister(result);
                return addrReg;
            }

            freeRegister(addrReg);
            return result;

        }
    }


    // TODO: Test, and check if structs of arrays work.
    @Override
    public Register visitArrayAccessExpr(ArrayAccessExpr arrayAccessExpr)
    {
        // Eventually visits VarExpr which fills in the value for globalVarDecl and returns
        // an address register, or contents.


        // Returns register for right hand side (i.e. loading immediate to register)
        arrayAccessRhs = true;
        Register rhsReg = arrayAccessExpr.expr2.accept(this);
        arrayAccessRhs = false;

        // Set flag to true so that VarExpr does not print lw instructions - need to
        // increment the address to access specific index before loading word.
        arrayAccessLhs = true;
        Register lhsReg = arrayAccessExpr.expr1.accept(this);
        arrayAccessLhs = false;

        boolean isGlobal = globalVarDecl.isGlobalVar;

        if (isGlobal) {

            if (acceptingLhs) {
                // Get offset. VisitAssign emits the sw instruction on the returned register
                writer.printf("\tmul %s, %s, 4\n", rhsReg, rhsReg);
                writer.printf("\tadd %s, %s, %s\n", lhsReg, lhsReg, rhsReg);
                freeRegister(rhsReg);
                return lhsReg;
                // Need to load word with the new register
            } else {
                Register result = getRegister();
                writer.printf("\tmul %s, %s, 4\n", rhsReg, rhsReg);
                writer.printf("\tadd %s, %s, %s\n", lhsReg, lhsReg, rhsReg);
                writer.printf("\tlw %s, (%s)\n", result, lhsReg);
                freeRegister(lhsReg);
                freeRegister(rhsReg);
                return result;
            }
        } else {
            if (acceptingLhs) {

                // Get offset. VisitAssign emits the sw instruction on the returned register
                writer.printf("\tmul %s, %s, 4\n", rhsReg, rhsReg);
                // Sub the offset from starting point of array. lhsReg stores the starting point
                writer.printf("\tsub %s, %s, %s\n", lhsReg, lhsReg, rhsReg);
                freeRegister(rhsReg);

                // Return location within stack
                return lhsReg;

                // Need to load word with the new register
            } else {
                Register result = getRegister();
                writer.printf("\tmul %s, %s, 4\n", rhsReg, rhsReg);
                // Sub the offset from starting point of array
                writer.printf("\tsub %s, %s, %s\n", lhsReg, lhsReg, rhsReg);

                // Load the data from address into a register
                writer.printf("\tlw %s, (%s)\n", result, lhsReg);
                freeRegister(lhsReg);
                freeRegister(rhsReg);
                return result;
            }

        }

    }


    @Override
    public Register visitBinOp(BinOp binOp) {
        Register lhsReg = binOp.expr1.accept(this);
        Register rhsReg = binOp.expr2.accept(this);
        Register result = getRegister();
        switch (binOp.op) {
            case ADD:
                writer.printf("\tadd %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;

            case SUB:
                writer.printf("\tsub %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;

            case MUL:
                writer.printf("\tmul %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;

            case DIV:
                writer.printf("\tdiv %s, %s\n", lhsReg.toString(), rhsReg.toString());
                writer.printf("\tmflo %s\n", result);
                break;

            case MOD:
                writer.printf("\tdiv %s, %s\n", lhsReg.toString(), rhsReg.toString());
                writer.printf("\tmfhi %s\n", result);
                break;

            case NE:
                writer.printf("\tsne %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;


            // Control flow operators/comparisons start here.
            case EQ:
                writer.printf("\tseq %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;

            case LT:
                writer.printf("\tslt %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;

            case GT:
                writer.printf("\tsgt %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;

            case GE:
                writer.printf("\tsge %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;

            case LE:
                writer.printf("\tsle %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;


                // TODO: Change this to use control flows if there is time at the end.
            case AND:
                writer.printf("\tand %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;

            case OR:
                writer.printf("\tor %s, %s, %s\n", result.toString(),
                        lhsReg.toString(), rhsReg.toString());
                break;


            default:
                break;

        }
        freeRegister(lhsReg);
        freeRegister(rhsReg);
        return result;
    }

    @Override
    public Register visitFunCallExpr(FunCallExpr funCallExpr) {

        // Built in functions
        if (funCallExpr.string.equals("print_i")) {
            writer.printf("\n\t# Printing Integer\n");
            Register print_i_reg = funCallExpr.expr.get(0).accept(this);

            writer.printf("\tli %s, %d\n", Register.v0.toString(), 1);
            writer.printf("\tadd %s, %s, $zero\n", Register.paramRegs[0],
                    print_i_reg);
            writer.printf("\tsyscall\n\n");
            freeRegister(print_i_reg);

        } else if (funCallExpr.string.equals("print_c")) {
            writer.printf("\n\t# Printing Character\n");

            Register print_c_reg = funCallExpr.expr.get(0).accept(this);
            writer.printf("\tli %s, %d\n", Register.v0.toString(), 11);
            writer.printf("\tadd %s, %s, $zero\n", Register.paramRegs[0],
                    print_c_reg);
            writer.printf("\tsyscall\n\n");
            freeRegister(print_c_reg);

        } else if (funCallExpr.string.equals("print_s")) {
            writer.printf("\n\t# Printing String\n");

            writer.printf("\tli %s, %d\n", Register.v0.toString(), 4);
            Register print_s_reg = funCallExpr.expr.get(0).accept(this);
            writer.printf("\tmove %s, %s\n", Register.paramRegs[0], print_s_reg.toString());
            writer.printf("\tsyscall\n\n");
            freeRegister(print_s_reg);


        } else if (funCallExpr.string.equals("read_i")) {
            Register result = getRegister();

            writer.printf("\n\t# Reading Integer\n");
            writer.printf("\tli %s, %d\n", Register.v0.toString(), 5);
            writer.printf("\tsyscall\n");
            writer.printf("\tmove %s, %s\n", result.toString(), Register.v0.toString());
            return result;

        } else if (funCallExpr.string.equals("read_c")) {
            Register result = getRegister();

            writer.printf("\n\t# Reading Character\n");
            writer.printf("\tli %s, %d\n", Register.v0.toString(), 12);
            writer.printf("\tsyscall\n");
            writer.printf("\tmove %s, %s\n", result.toString(), Register.v0.toString());
            return result;

            // TODO: Check if mcmalloc is correct
        } else if (funCallExpr.string.equals("mcmalloc")) {

            writer.printf("\n\t# Allocating Memory\n");
            writer.printf("\tli %s, %d\n", Register.v0.toString(), 9);

            // Goes into strLiteral, and emits an li instruction that I need to move to v0
            Register size = funCallExpr.expr.get(0).accept(this);
            writer.printf("\tmove %s, %s\n", Register.paramRegs[0], size);
            writer.printf("\tsyscall\n");
            freeRegister(size);

        } else {

            // Store original sp so callee function can return to original state.
            writer.printf("\tadd %s, %s, $zero\n", Register.fp, Register.sp);

            // Push arguments onto stack
            writer.printf("\taddi %s, %s, -%d\n", Register.sp, Register.sp,
                    funCallExpr.expr.size()*4);

            int counter = 0;
            for (Expr e : funCallExpr.expr) {
                Register result = e.accept(this);
                writer.printf("\tsw %s %d(%s)\n", result, counter, Register.sp);
                freeRegister(result);
                counter += 4;
            }

            writer.printf("\tjal %s\n", funCallExpr.string);

            // Set sp back to normal after callee function is completed.
            writer.printf("\tadd %s, %s, $zero\n", Register.sp, Register.fp);

            return Register.v1;

        }
        return null;
    }

    @Override
    public Register visitIntLiteral(IntLiteral intLiteral) {

        Register reg = getRegister();
        writer.printf("\tli %s, %d\n", reg, intLiteral.int1);
        return reg;
    }

    @Override
    public Register visitChrLiteral(ChrLiteral chrLiteral) {
        int charAscii = (int) chrLiteral.char1;
        Register reg = getRegister();

        writer.printf("\tli %s, %d\n", reg, charAscii);
        return reg;
    }

    @Override
    public Register visitStrLiteral(StrLiteral strLiteral) {
        Register reg = getRegister();

        writer.printf("\tla %s, %s\n", reg.toString(), strLiteral.label);
        return reg;
    }

    @Override
    public Register visitArrayType(ArrayType arrayType) {
        return null;
    }

    @Override
    public Register visitPointerType(PointerType pointerType) {
        return null;
    }
}


