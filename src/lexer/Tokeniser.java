package lexer;

import lexer.Token.TokenClass;

import java.io.EOFException;
import java.io.IOException;

/**
 * @author cdubach
 */
public class Tokeniser {

	private Scanner scanner;

	private int error = 0;

	private boolean commentOpen = false;

	public int getErrorCount() {
		return this.error;
	}

	public Tokeniser(Scanner scanner) {
		this.scanner = scanner;
	}

	private void error(char c, int line, int col) {
		System.out.println("Lexing error: unrecognised character (" + c
				+ ") at " + line + ":" + col);
		error++;
	}

	public Token nextToken() {
		Token result;
		try {
			result = next();
		} catch (EOFException eof) {
			// end of file, nothing to worry about, just return EOF token
			return new Token(TokenClass.EOF, scanner.getLine(),
					scanner.getColumn());
		} catch (IOException ioe) {
			ioe.printStackTrace();
			// something went horribly wrong, abort
			System.exit(-1);
			return null;
		}
		return result;
	}
	


	/*
	 * To be completed
	 */
	private Token next() throws IOException {

		int line = scanner.getLine();
		int column = scanner.getColumn();

		// get the next character
		char c = scanner.next();

		// skip white spaces
		if (Character.isWhitespace(c))
			return next();

		// Arithmetic
		if (c == '+')
			return new Token(TokenClass.PLUS, line, column);
		if (c == '-')
			return new Token(TokenClass.MINUS, line, column);
		if (c == '*')
			return new Token(TokenClass.ASTERIX, line, column);

		// Comments and DIV
		if (c == '/') {

			// If the next character is not * or /, then it has to be
			// a division
			if (scanner.peek() != '*' && scanner.peek() != '/') {
				return new Token(TokenClass.DIV, line, column);
				// Check for inline comments
			} else if (scanner.peek() == '/') {

				// Keep consuming characters till we encounter the new line
				while (c != '\n') {
					c = scanner.next();
				}
				return next();

				// Start of block comment, so ignore everything until we
				// encounter close comment
			} else if (scanner.peek() == '*') {

				// currently pointing to /, so consume /, and then consume the
				// *, and set c to be the character after *
				scanner.next();
				c = scanner.peek();

				// Consume everything until we see end block
				while (true) {
					
					// Encountered *, so consume it
					if (c == '*') {
						
						// Consume the currently peeked character, and peek again
						try {
							scanner.next();
							c = scanner.peek();
						} catch (EOFException e) {
							error(c, line, column);
							return new Token(TokenClass.INVALID, line, column);
						}
						
						if (c == '/') {
							scanner.next();
							break;
						} 
						
					// Bugfix, wouldn't work on multiline comments without this
					} 
					
					try {
						scanner.next();
						c = scanner.peek();
					} catch (EOFException e) {
						error(c, line, column);
						return new Token(TokenClass.INVALID, line, column);
					}
					
				}

			}
			return next();
		}

		if (c == '%')
			return new Token(TokenClass.REM, line, column);

		// Delimiters
		if (c == '.')
			return new Token(TokenClass.DOT, line, column);
		if (c == '{')
			return new Token(TokenClass.LBRA, line, column);
		if (c == '}')
			return new Token(TokenClass.RBRA, line, column);
		if (c == '(')
			return new Token(TokenClass.LPAR, line, column);
		if (c == ')')
			return new Token(TokenClass.RPAR, line, column);
		if (c == '[')
			return new Token(TokenClass.LSBR, line, column);
		if (c == ']')
			return new Token(TokenClass.RSBR, line, column);
		if (c == ';')
			return new Token(TokenClass.SC, line, column);
		if (c == ',')
			return new Token(TokenClass.COMMA, line, column);

		// Logical Operators
		if (c == '&') {
			if (scanner.peek() == '&') {
				scanner.next();
				return new Token(TokenClass.AND, line, column);
			}
			error(c, line, column);
			return new Token(TokenClass.INVALID, line, column);
		}

		if (c == '|') {
			if (scanner.peek() == '|') {
				scanner.next();
				return new Token(TokenClass.OR, line, column);
			}
			error(c, line, column);
			return new Token(TokenClass.INVALID, line, column);
		}

		// Comparisons
		if (c == '=') {			
			if (scanner.peek() == '=') {
				scanner.next();
				return new Token(TokenClass.EQ, line, column);
			}
			return new Token(TokenClass.ASSIGN, line, column);
		}

		if (c == '<') {
			if (scanner.peek() == '=') {
				scanner.next();
				return new Token(TokenClass.LE, line, column);
			}
			return new Token(TokenClass.LT, line, column);
		}

		if (c == '>') {
			if (scanner.peek() == '=') {
				scanner.next();
				return new Token(TokenClass.GE, line, column);
			}
			return new Token(TokenClass.GT, line, column);
		}

		if (c == '!') {
			if (scanner.peek() == '=') {
				scanner.next();
				return new Token(TokenClass.NE, line, column);
			}
			error(c, line, column);
			return new Token(TokenClass.INVALID, line, column);
		}

		
		// Identifier
		if (Character.isLetter(c) || c == '_') {
			StringBuilder id = new StringBuilder();
			id.append(c);
			c = scanner.peek();

			while (Character.isLetterOrDigit(c) || c == '_') {
				id.append(c);
				scanner.next();
				c = scanner.peek();
			}

			String token = id.toString();

			// Types
			if (token.equals("int")) {
				return new Token(TokenClass.INT, line, column);
			} else if (token.equals("void")) {
				return new Token(TokenClass.VOID, line, column);
			} else if (token.equals("char")) {
				return new Token(TokenClass.CHAR, line, column);

				// Keywords
			} else if (token.equals("char")) {
				return new Token(TokenClass.CHAR, line, column);
			} else if (token.equals("if")) {
				return new Token(TokenClass.IF, line, column);
			} else if (token.equals("else")) {
				return new Token(TokenClass.ELSE, line, column);
			} else if (token.equals("while")) {
				return new Token(TokenClass.WHILE, line, column);
			} else if (token.equals("return")) {
				return new Token(TokenClass.RETURN, line, column);
			} else if (token.equals("sizeof")) {
				return new Token(TokenClass.SIZEOF, line, column);
			} else if (token.equals("struct")) {
				return new Token(TokenClass.STRUCT, line, column);
			} else {
				return new Token(TokenClass.IDENTIFIER, token, line, column);
			}
		}

		// #include
		if (c == '#') {

			// Form the include string
			StringBuilder sb = new StringBuilder();
			sb.append("#");

			// Check against this string to ensure input is correct
			String includeStr = "#include";
			c = scanner.peek();

			// Check characters against the letters in "#include"
			for (int i = 1; i < 8; i++) {
				if (includeStr.charAt(i) == c) {
					sb.append(c);
					scanner.next();
					c = scanner.peek();
				}
			}

			if (sb.toString().equals("#include")) {
				return new Token(TokenClass.INCLUDE, line, column);
			} else {
				error(c, line, column);
				return new Token(TokenClass.INVALID, line, column);
			}

		}

		// Int Literal
		if (Character.isDigit(c)) {
			StringBuilder intLiteral = new StringBuilder();
			intLiteral.append(c);
			c = scanner.peek();

			while (Character.isDigit(c)) {
				intLiteral.append(c);
				scanner.next();
				c = scanner.peek();
			}

			return new Token(TokenClass.INT_LITERAL, intLiteral.toString(),
					line, column);
		}

		// Character Literal
		if (c == '\'') {
			StringBuilder charLiteral = new StringBuilder();
			c = scanner.next();
			while (c != '\'') { 
				// Deal with the list of escape characters
				if (c == '\\' && scanner.peek() == '"') {
					charLiteral.append('\"');
					scanner.next();
					c = scanner.peek();
					
					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					}
					
				} else if (c == '\\' && scanner.peek() == '\'') {
					charLiteral.append('\'');
					scanner.next();
					c = scanner.peek();

					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					}
					
				} else if (c == '\\' && scanner.peek() == '\\') {
					charLiteral.append('\\');
					scanner.next();
					c = scanner.peek();
					
					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					}
				} else if (c == '\\' && scanner.peek() == 'n') {
					charLiteral.append('\n');
					scanner.next();
					c = scanner.peek();
					
					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					}
				} else if (c == '\\' && scanner.peek() == 't') {
					charLiteral.append('\t');
					scanner.next();
					c = scanner.peek();
					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					}
					
				} else if (c == '\\' && scanner.peek() == 'r') {
					charLiteral.append('\r');
					scanner.next();
					c = scanner.peek();
					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					}
					
				} else if (c == '\\' && scanner.peek() == 'b') {
					charLiteral.append('\b');
					scanner.next();
					c = scanner.peek();
					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					}
					
				} else if (c == '\\' && scanner.peek() == 'f') {
					charLiteral.append('\f');
					scanner.next();
					c = scanner.peek();
					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					}
					
					
				// Unescaped backscape encountered, 	
				} else if (c == '\\') {
					while (scanner.peek() != '\'') {
						scanner.next();
					}
					scanner.next();
					error(c, line, column);
					return new Token(TokenClass.INVALID, line, column);
					
				// Else check to see if we encountered a character	
				} else if (c >= 32 && c <= 127) {
					
					charLiteral.append(c);
					c = scanner.peek();
					if (c == '\'') {
						scanner.next();
						return new Token(TokenClass.CHAR_LITERAL,
								charLiteral.toString(), line, column);
					} else {
						while (scanner.peek() != '\'') {
							scanner.next();
						}
						scanner.next();
						error(c, line, column);
						return new Token(TokenClass.INVALID, line, column);
					}
						

				} else {
					error(c, line, column);
					return new Token(TokenClass.INVALID, line, column);
				}
					
			}
	
		}
		

		// String Literal
		if (c == '\"') {
			StringBuilder stringLiteral = new StringBuilder();
			c = scanner.next();

			while (c != '\"' && c!= 10 &&  scanner.peek() != 10) {

				char next = scanner.peek();

				// If we encounter an escape char, append it
				// and consume both the \ and the character (hence the 2 nexts).
				if (c == '\\' && next == '\"') {
					stringLiteral.append('\"');
					scanner.next();
					c = scanner.next();

				} else if (c == '\\' && next == '\\') {
					stringLiteral.append("\\");
					scanner.next();
					c = scanner.next();

				} else if (c == '\\' && next == '\'') {
					stringLiteral.append('\'');
					scanner.next();
					c = scanner.next();
					
				} else if (c == '\\' && next == 'b') {
					stringLiteral.append('\b');
					scanner.next();
					c = scanner.next();

				} else if (c == '\\' && next == 'r') {
					stringLiteral.append('\r');
					scanner.next();
					c = scanner.next();
				} else if (c == '\\' && next == 'f') {
					stringLiteral.append('\f');
					scanner.next();
					c = scanner.next();

				} else if (c == '\\' && next == 'n') {
					stringLiteral.append("\\");
					stringLiteral.append("n");
					System.out.println("here");
					scanner.next();
					c = scanner.next();

				} else if (c == '\\' && next == 't') {
					stringLiteral.append("\\");
					stringLiteral.append("t");
					scanner.next();
					c = scanner.next();

					// If we encounter a \ that does not follow an escape
					// character,
					// then this is invalid and everything up till the closing
					// quotation
					// must be reported invalid. Hence consume until "
				} else if (c == '\\') {

					// Will scan and consume till a second
					// " is encountered. If " is
					// not encountered e.g. in " \, then EOF error will occur
					// eventually
					while (scanner.peek() != '\"') {
						scanner.next();
					}
					
					scanner.next();
					error(c, line, column);
					return new Token(TokenClass.INVALID, line, column);

					// If it's not an escape (or illegally escaped character),
					// then it must be a
					// simple legal character, and can simply be appended, then
					// move onto the next
					// letter in the file stream.
				} else {
					stringLiteral.append(c);
					c = scanner.next();
				}
			}

			// A quotation without a backslash prior to it has been encountered,
			// so it must be the closing
			// quotation, so tokenize as string.
			if (c == '"') {
				return new Token(TokenClass.STRING_LITERAL,
						stringLiteral.toString(), line, column);

				// No closing ", so report an error.
			} else {
				error(c, line, column);
				return new Token(TokenClass.INVALID, line, column);
			}
		}

		// if we reach this point, it means we did not recognise a valid token
		error(c, line, column);
		return new Token(TokenClass.INVALID, line, column);
	}

}