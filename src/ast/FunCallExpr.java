package ast;

import java.util.List;

public class FunCallExpr extends Expr {
	
	public String string;
	public List<Expr> expr;
	public FunDecl fd; // Filled in by name analysis
	
	public FunCallExpr(String string, List<Expr> expr) {
		this.string = string;
		this.expr = expr;
	}

	public <T> T accept(ASTVisitor<T> v) {
        return v.visitFunCallExpr(this);
    }
	
	

}
