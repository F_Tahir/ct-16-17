package ast;

import java.util.List;

/**
 * @author cdubach
 */
public class StructType implements Type {


    public String string;
    public List<VarDecl> vars;

    
    public StructType(String string, List<VarDecl> vars) {
    	this.string = string;
    	this.vars = vars;
    }
    public <T> T accept(ASTVisitor<T> v) {
        return v.visitStructType(this);
    }

}
