package ast;


public class ArrayType implements Type {

    public Type type;
    public int int1;

    public ArrayType(Type type, int int1) {
        this.type = type;
        this.int1 = int1;
    }

    @Override
    public <T> T accept(ASTVisitor<T> v) {
        return v.visitArrayType(this);
    }


}
