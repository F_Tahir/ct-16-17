package ast;

public class VarDecl implements ASTNode {
    public final Type type;
    public final String varName;
    public int offset; // Store the offset for code generation
    public boolean isGlobalVar;

    public VarDecl(Type type, String varName) {
	    this.type = type;
	    this.varName = varName;
	    this.isGlobalVar = false;
    }

     public <T> T accept(ASTVisitor<T> v) {
	return v.visitVarDecl(this);
    }
}
