package ast;

public class ArrayAccessExpr extends Expr {

	public Expr expr1;
	public Expr expr2;

	public ArrayAccessExpr(Expr expr1, Expr expr2) {
		this.expr1 = expr1;
		this.expr2 = expr2;
	}

	@Override
	public <T> T accept(ASTVisitor<T> v) {
		return v.visitArrayAccessExpr(this);
	}



}
