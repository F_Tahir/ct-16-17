package ast;

public class FieldAccessExpr extends Expr {

	public Expr expr;
	public String string;

	public FieldAccessExpr(Expr expr, String string) {
		this.expr = expr;
		this.string = string;
			
	}
	
	@Override
	public <T> T accept(ASTVisitor<T> v) {
    	return v.visitFieldAccessExpr(this);
    }

}
