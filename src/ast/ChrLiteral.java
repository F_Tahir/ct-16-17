package ast;

public class ChrLiteral extends Expr {
	public char char1;
	
	public ChrLiteral(char char1) {
		this.char1 = char1;
	}
	
	public <T> T accept(ASTVisitor<T> v) {
        return v.visitChrLiteral(this);
    }

}
