package ast;

public class IntLiteral extends Expr {
	public int int1;
	
	public IntLiteral(int int1) {
		this.int1 = int1;
	}
	
	public <T> T accept(ASTVisitor<T> v) {
        return v.visitIntLiteral(this);
    }

}
