package ast;

public class StrLiteral extends Expr {
	public String string;
	public String label; // Used to access .data segment in generated code
	
	public StrLiteral(String string) {
		this.string = string;
	}
	
	public <T> T accept(ASTVisitor<T> v) {
        return v.visitStrLiteral(this);
    }

}
