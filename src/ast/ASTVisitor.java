package ast;

public interface ASTVisitor<T> {
    public T visitBaseType(BaseType bt);
    public T visitStructType(StructType st);
    public T visitBlock(Block b);
    public T visitFunDecl(FunDecl p);
    public T visitProgram(Program p);
    public T visitVarDecl(VarDecl vd);
    public T visitVarExpr(VarExpr v);
	public T visitReturn(Return rt);
	public T visitAssign(Assign assign);
	public T visitIf(If if1);
	public T visitWhile(While while1);
	public T visitExprStmt(ExprStmt exprStmt);
	public T visitTypecastExpr(TypecastExpr typecastExpr);
	public T visitSizeOfExpr(SizeOfExpr sizeOfExpr);
	public T visitValueAtExpr(ValueAtExpr valueAtExpr);
	public T visitFieldAccessExpr(FieldAccessExpr fieldAccessExpr);
	public T visitArrayAccessExpr(ArrayAccessExpr arrayAccessExpr);
	public T visitBinOp(BinOp binOp);
	public T visitFunCallExpr(FunCallExpr funCallExpr);
	public T visitIntLiteral(IntLiteral intLiteral);
	public T visitChrLiteral(ChrLiteral chrLiteral);
	public T visitStrLiteral(StrLiteral strLiteral);
	public T visitArrayType(ArrayType arrayType);
	public T visitPointerType(PointerType pointerType);
    


    // to complete ... (should have one visit method for each concrete AST node class)
}
