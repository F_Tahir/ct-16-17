package ast;

public class While extends Stmt {
	
	public Expr expr;
	public Stmt stmt;
	
	public While(Expr expr, Stmt stmt) {
		this.expr = expr;
		this.stmt = stmt;
	}
	
	@Override
	public <T> T accept(ASTVisitor<T> v) {
    	return v.visitWhile(this);
    }

}
