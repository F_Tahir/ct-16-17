package ast;

import java.io.PrintWriter;

public class ASTPrinter implements ASTVisitor<Void> {

    private PrintWriter writer;

    public ASTPrinter(PrintWriter writer) {
            this.writer = writer;
    }

    @Override
    public Void visitBlock(Block b) {
        writer.print("Block(");
        
        String delimiter = "";
        for (VarDecl vd : b.params) {
        	writer.print(delimiter);
            delimiter = ",";
            vd.accept(this);
        }
        
        for (Stmt st : b.stmts) {
        	writer.print(delimiter);
            delimiter = ",";
            st.accept(this);
        }
        
        writer.print(")");
        return null;
    }

    @Override
    public Void visitFunDecl(FunDecl fd) {
        writer.print("FunDecl(");
        fd.type.accept(this);
        writer.print(","+fd.name+",");
        for (VarDecl vd : fd.params) {
            vd.accept(this);
            writer.print(",");
        }
        fd.block.accept(this);
        writer.print(")");
        return null;
    }
    
    
    @Override
    public Void visitProgram(Program p) {
        writer.print("Program(");
        String delimiter = "";
        for (StructType st : p.structTypes) {
            writer.print(delimiter);
            delimiter = ",";
            st.accept(this);
        }
        for (VarDecl vd : p.varDecls) {
            writer.print(delimiter);
            delimiter = ",";
            vd.accept(this);
        }
        for (FunDecl fd : p.funDecls) {
            writer.print(delimiter);
            delimiter = ",";
            fd.accept(this);
        }
        writer.print(")");
	    writer.flush();
        return null;
    }

    // Changed this to make parsing of PointerType possible. 
    // Calling accept lets us visit the PointerType node
    @Override
    public Void visitVarDecl(VarDecl vd){
        writer.print("VarDecl(");
        vd.type.accept(this);
        writer.print(","+vd.varName);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitVarExpr(VarExpr v) {
        writer.print("VarExpr(");
        writer.print(v.name);
        writer.print(")");
        return null;
    }

    @Override
    public Void visitBaseType(BaseType bt) {
        writer.print(bt.toString());
        return null;
    }

    @Override
    public Void visitStructType(StructType st) {
        writer.print("StructType(");
        writer.print(st.string);
        String delimiter = ",";
        
        // Loop through st.vars (it's an ArrayList), and
        // set each instance to vd
        for (VarDecl vd : st.vars) {
        	writer.print(delimiter);
        	vd.accept(this);
        }
        writer.print(")");
        return null;
    }

	@Override
	public Void visitReturn(Return rt) {
		writer.print("Return(");
		if (rt.expr == null) {
			writer.print(")");
			
		// Not null, so print expr between parentheses	
		} else {
			rt.expr.accept(this);
			writer.print(")");
		}
		return null;
	}

	@Override
	public Void visitAssign(Assign as) {
		writer.print("Assign(");
		as.expr1.accept(this);
		writer.print(",");
		as.expr2.accept(this);
		writer.write(")");
		return null;
	}

	@Override
	public Void visitIf(If if1) {
		writer.print("If(");
		if1.expr.accept(this);
		writer.print(",");
		if1.stmt1.accept(this);
		
		
		if (if1.stmt2 != null) {
			writer.print(",");
			if1.stmt2.accept(this);
		}
		writer.print(")");
		return null;
	}

	@Override
	public Void visitWhile(While while1) {
		writer.print("While(");
		while1.expr.accept(this);
		writer.print(",");
		while1.stmt.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitExprStmt(ExprStmt exprStmt) {
		writer.print("ExprStmt(");
		exprStmt.expr.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitTypecastExpr(TypecastExpr typecastExpr) {
		writer.print("TypecastExpr(");
		typecastExpr.type.accept(this);
		writer.print(",");
		typecastExpr.expr.accept(this);
		writer.print(")");
		return null;
	}
	

	@Override
	public Void visitSizeOfExpr(SizeOfExpr sizeOfExpr) {
		writer.print("SizeOfExpr(");
		sizeOfExpr.type.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitValueAtExpr(ValueAtExpr valueAtExpr) {
		writer.print("ValueAtExpr(");
		valueAtExpr.expr.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitFieldAccessExpr(FieldAccessExpr fieldAccessExpr) {
		writer.print("FieldAccessExpr(");
		fieldAccessExpr.expr.accept(this);
		writer.print(",");
		writer.print(fieldAccessExpr.string);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitArrayAccessExpr(ArrayAccessExpr arrayAccessExpr) {
		writer.print("ArrayAccessExpr(");
		arrayAccessExpr.expr1.accept(this);
		writer.print(",");
		arrayAccessExpr.expr2.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitBinOp(BinOp binOp) {
		writer.print("BinOp(");
		binOp.expr1.accept(this);
		writer.print(",");
		writer.print(binOp.op);
		writer.print(",");
		binOp.expr2.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitFunCallExpr(FunCallExpr funCallExpr) {
		writer.print("FunCallExpr(");
		writer.print(funCallExpr.string);
		
		for (Expr e : funCallExpr.expr) {
			writer.print(",");
			e.accept(this);
		}
		writer.print(")");
		return null;
	}

	@Override
	public Void visitIntLiteral(IntLiteral intLiteral) {
		writer.print("IntLiteral(");
		writer.print(intLiteral.int1);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitChrLiteral(ChrLiteral chrLiteral) {
		writer.print("ChrLiteral(");
		writer.print(chrLiteral.char1);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral strLiteral) {
		writer.print("StrLiteral(");
		writer.print(strLiteral.string);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitArrayType(ArrayType arrayType) {
		writer.print("ArrayType(");
		arrayType.type.accept(this);
		writer.print(",");
		writer.print(arrayType.int1);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitPointerType(PointerType pointerType) {
		System.out.println("Going into visitPointerType");
		writer.print("PointerType(");
		pointerType.type.accept(this);
		writer.print(")");
		return null;
	}

    // to complete ...
    
}
