package ast;

public class TypecastExpr extends Expr {

	public Type type;
	public Expr expr;
	
	public TypecastExpr(Type type, Expr expr) {
		this.type = type;
		this.expr = expr;
	}
	
	@Override
	public <T> T accept(ASTVisitor<T> v) {
    	return v.visitTypecastExpr(this);
    }
}
