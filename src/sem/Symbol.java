package sem;

import ast.FunDecl;
import ast.VarDecl;

public abstract class Symbol {
    public String name;


    public Symbol(String name) {
        this.name = name;
    }

    public abstract boolean isVar();

    public abstract boolean isFunc();
}


// Unsure whether these should be separate classes (but I think this should work
// TODO: Test whether this implementation works, otherwise create a separate class
class VarSymbol extends Symbol {
    VarDecl vd;

    VarSymbol(VarDecl vd) {
        super(vd.varName);
        this.vd = vd;
    }

    public boolean isVar() {
        return true;
    }

    public boolean isFunc() {
        return false;
    }
}


class FuncSymbol extends Symbol {
    FunDecl p;

    FuncSymbol(FunDecl p) {
        super(p.name);
        this.p = p;
    }

    public boolean isVar() {
        return false;
    }

    public boolean isFunc() {
        return true;
    }
}


