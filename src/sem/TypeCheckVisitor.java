package sem;

import ast.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class TypeCheckVisitor extends BaseSemanticVisitor<Type> {

    private Type returnType;

    // This hashmap stores for each struct var decl, the identifier as the key, and the variable declarations within the struct
    // as its values
    public static Map<String, List<VarDecl>> structVarData = new HashMap<String, List<VarDecl>>();




    private boolean compareTypes(Type type, Type type1) {

        // Without this check, if I accept a PointerType(INT), and an ArrayType(INT) down to its basetype INT's, this
        // function will return true which is wrong. These if statements check for that, and if the high level types are
        // different, then return false.
        if (type instanceof PointerType && type1 instanceof StructType) {
            return false;
        } else if (type instanceof PointerType && type1 instanceof ArrayType) {
            return false;
        } else if (type instanceof ArrayType && type1 instanceof PointerType) {
            return false;
        } else if (type instanceof ArrayType && type1 instanceof StructType) {
            return false;
        } else if (type instanceof StructType && type1 instanceof ArrayType) {
            return false;
        } else if (type instanceof StructType && type1 instanceof PointerType) {
            return false;
        }

        // If both types are struct type, we have a match. This is because a StructType. This is because structtype's can
        // have a pointer type or array type parent, however if the parents of the structtype are different, then this
        // is dealt with at the start of the function.
        if (type instanceof StructType && type1 instanceof StructType) {
            String type1Ident = ((StructType) type).string;
            String type2Ident = ((StructType) type1).string;

            if (type1Ident.equals(type2Ident)) {
                return true;
            } else {
                error("Expected \"struct " + type1Ident + "\", but passed in \"struct " + type2Ident + "\" as an argument " +
                        "to some function instead.");
                return false;
            }

        }

        // If both are ArrayTypes, need to compare size of arrays
        if (type instanceof ArrayType) {
            int t1Index = ((ArrayType) type).int1;
            int t2Index = ((ArrayType) type1).int1;

            if (t1Index != t2Index) {
                return false;
            }
        }

        while (true) {
            if (type == BaseType.INT || type == BaseType.CHAR || type1 == BaseType.INT || type1 == BaseType.CHAR
                    || type == null || type1 == null) {
                break;
            }
            type = getLowerType(type);
            type1 = getLowerType(type1);
        }
        return type == type1;
    }

    // If we have ArrayType(INT, 15)..., this method would return BaseType.INT
    private Type getLowerType(Type t) {
        if (t instanceof BaseType) {
            return t;
        } else if (t instanceof ArrayType) {
            return ((ArrayType) t).type;
        } else if (t instanceof PointerType) {
            return ((PointerType) t).type;
        } // TODO: Check if it's an instance of StructType
        return null;
    }

    // If we have nested, e.g. ArrayType(PointerType(INT...), returns INT by recursing through
    private Type getBaseType(Type t) {

        while (true) {
            if (t == BaseType.INT || t == BaseType.VOID || t == BaseType.CHAR || t == null) {
                break;
            }

            t = getLowerType(t);
        }
        return t;
    }

    @Override
    public Type visitBaseType(BaseType bt) {
        return bt;
    }

    @Override
    public Type visitStructType(StructType st) {

        if (st.vars.size() > 0) {
            structVarData.put(st.string, st.vars);
        } else {
            return st;
        }
        return null;
    }

    @Override
    public Type visitBlock(Block b) {

        for (VarDecl vd : b.params) {
            vd.accept(this);
        }

        for (Stmt s : b.stmts) {
            s.accept(this);
        }
        return null;
    }

    @Override
    public Type visitFunDecl(FunDecl p) {

        returnType = p.type;
        for (VarDecl vd : p.params) {
            vd.accept(this);
        }

        p.block.accept(this);

        return p.type;
    }


    @Override
    public Type visitProgram(Program p) {
        for (StructType st : p.structTypes) {
            st.accept(this);
        }

        for (VarDecl vd : p.varDecls) {
            // isGlobalVar is false by default, but change it to true if its being defined in global scope.
            // This is used for Code generation.
            vd.isGlobalVar = true;
            vd.accept(this);
        }

        for (FunDecl fd : p.funDecls) {
            fd.accept(this);
        }

        return null;
    }

    @Override
    public Type visitVarDecl(VarDecl vd) {
        if (vd.type == BaseType.VOID) {
            error("VarDecl cannot have a void type!");
            return null;
        }

        return vd.type;
    }

    @Override
    public Type visitVarExpr(VarExpr v) {
        if (v.vd != null) {
            v.type = v.vd.type;
            return v.vd.type;
        }
        return null;
    }

    @Override
    public Type visitReturn(Return rt) {
        if (rt.expr == null) {
            if (returnType == BaseType.VOID) {
                return BaseType.VOID;
            } else {
                return null;
            }

        } else {
            Type type = rt.expr.accept(this);
            if (compareTypes(returnType, type)) {
                return type;
            } else {
                error("Function return type does not match expected return type!");
                return null;
            }
        }
    }

    @Override
    public Type visitAssign(Assign assign) {
        // Store lhs of expression
        Expr exp = assign.expr1;

        Type lhs = assign.expr1.accept(this);
        Type rhs = assign.expr2.accept(this);

        // Task 6 of part 2.
        if (!(exp instanceof VarExpr) && !(exp instanceof ArrayAccessExpr) && !(exp instanceof ValueAtExpr) &&
                !(exp instanceof FieldAccessExpr)) {
            error("Left-hand side of assign must be an Array Access, Pointer expression, Variable expression, or" +
                    "field access expression!");
            return null;
        }
        if (lhs == BaseType.VOID) {
            error("Left-hand side of assign cannot be of type void!");
            return null;

        } else if (lhs instanceof ArrayType) {
            error("Left-hand side of assign cannot be of type ArrayType!");
            return null;

            // lhs and rhs equal and not of type void or ArrayType, so it's fine
        } else if (compareTypes(lhs, rhs)) {
            return lhs;
        }

        error("Left-hand-side and right-hand-side of Assign are not same type!");
        return null;
    }

    @Override
    public Type visitIf(If if1) {
        Type expr = if1.expr.accept(this);

        if (expr == BaseType.INT) {
            if1.stmt1.accept(this);

            if (if1.stmt2 != null) {
                if1.stmt2.accept(this);
                return expr;
            }
        } else {
            System.out.println(if1);
            error("If statement's expression must be an integer!");
            return null;
        }
        return expr;
    }

    @Override
    public Type visitWhile(While while1) {
        Type expr = while1.expr.accept(this);
        System.out.println(expr);

        if (expr == BaseType.INT) {
            while1.stmt.accept(this);
        } else {
            error("While statement's expression must be an integer!");
            return null;
        }
        return expr;
    }

    @Override
    public Type visitExprStmt(ExprStmt exprStmt) {
        return exprStmt.expr.accept(this);
    }

    @Override
    public Type visitTypecastExpr(TypecastExpr typeCastExpr) {
        Type castedType = typeCastExpr.type.accept(this);
        Type exprType = typeCastExpr.expr.accept(this);

        // Char to Int typecasting - e.g char a; a = 'c'; int d; d = (int) a;
        if (castedType == BaseType.INT && exprType == BaseType.CHAR) {
            typeCastExpr.type = BaseType.INT;
            return typeCastExpr.type;
        }

        // Array to Pointer typecasting
        else if (castedType instanceof PointerType && exprType instanceof ArrayType) {
            Type casted = ((PointerType) castedType).type;
            Type expr = ((ArrayType) exprType).type;
            if (casted == expr) {
                typeCastExpr.type = castedType;
                return typeCastExpr.type;
            } else {
                error("Illegal casting. Cannot cast " + expr + " to " + casted);
                return null;
            }
        }

        // pointer to pointer
        else if (castedType instanceof PointerType && exprType instanceof PointerType) {
            typeCastExpr.type = castedType;
            return typeCastExpr.type;
        }

        // Typecast is not the same as one of our rules so throw error
        else {
            error("Typecast from " + castedType + " to " + exprType + " is invalid!");
        }
        return null;
    }

    @Override
    public Type visitSizeOfExpr(SizeOfExpr sizeOfExpr) {
        return BaseType.INT;
    }

    @Override
    public Type visitValueAtExpr(ValueAtExpr valueAtExpr) {
        Type type = valueAtExpr.expr.accept(this);

        if (!(type instanceof PointerType)) {
            error("ValueAtExpr must have a PointerType!");
            return null;
        }
        valueAtExpr.type = getBaseType(type);
        return valueAtExpr.type;
    }

    @Override
    public Type visitFieldAccessExpr(FieldAccessExpr fieldAccessExpr) {

        // TODO: Check to see where fieldAccessExpr's type field is being assigned
        // Expr is the name of the structure (as defined in abstract_grammar.txt)
        Type structType = fieldAccessExpr.expr.accept(this);

        if (structType instanceof StructType) {
            List<VarDecl> listOfFields = new ArrayList<>();

            String structName = ((StructType) structType).string;

            if (structVarData.containsKey(structName)) {
                listOfFields = structVarData.get(structName);
            } else {
                error(structName + " does not exist as a struct. Declare it first before using it.");
                return null;
            }

            for (VarDecl vd : listOfFields) {

                if (vd.varName.equals(fieldAccessExpr.string)) {
                    return vd.type;
                }
            }
            error("Trying to access a field in " + structName + " that does not exist!");
            return null;
        } else {
            error("Field access with a non-struct type is invalid!");
            return null;
        }
    }

    @Override
    public Type visitArrayAccessExpr(ArrayAccessExpr arrayAccessExpr) {

        Type ident = arrayAccessExpr.expr1.accept(this);
        Type index = arrayAccessExpr.expr2.accept(this);

        if (!(ident instanceof ArrayType || ident instanceof PointerType)) {
            error("No ArrayType or PointerType found in ArrayAccessExpr!");
            return null;
        } else if (index != BaseType.INT) {
            error("Array access index expects an INT, but INT not found!");
            return null;
        }
        arrayAccessExpr.type = getLowerType(ident);
        return getBaseType(ident);
    }


    @Override
    public Type visitBinOp(BinOp binOp) {
        Type lhsT = binOp.expr1.accept(this);
        Type rhsT = binOp.expr2.accept(this);

        // TODO: May need to move LT, LE, GT, GE so that it also allows chars
        if (binOp.op == Op.ADD || binOp.op == Op.SUB || binOp.op == Op.MUL || binOp.op == Op.DIV
                || binOp.op == Op.MOD || binOp.op == Op.LT || binOp.op == Op.LE || binOp.op == Op.GT || binOp.op == Op.GE
                || binOp.op == Op.OR || binOp.op == Op.AND) {

            if (lhsT == BaseType.INT && rhsT == BaseType.INT) {
                binOp.type = BaseType.INT;
                return BaseType.INT;
            } else {
                error("Arithmetic operators have incorrect types!");
            }
        }

        if (binOp.op == Op.NE || binOp.op == Op.EQ) {
            if (compareTypes(lhsT, rhsT)) {
                binOp.type = BaseType.INT;
                return BaseType.INT;
            } else {
                error("Not Equals or Equals have incorrect types!");
                return null;
            }
        }
        return binOp.type;
    }

    @Override
    public Type visitFunCallExpr(FunCallExpr funCallExpr) {
        if (funCallExpr.string.equals("read_i")) {
            if (funCallExpr.expr.isEmpty()) {
                return BaseType.INT;
            } else {
                error("read_i() takes no parameters!");
                return null;
            }
        } else if (funCallExpr.string.equals("read_c")) {
            if (funCallExpr.expr.isEmpty()) {
                return BaseType.CHAR;
            } else {
                error("read_c() takes no parameters!");
                return null;
            }

        } else if (funCallExpr.string.equals("print_i")) {
            if (funCallExpr.expr.size() != 1) {
                error("print_i() should take exactly one argument!");
                return null;
            } else if (funCallExpr.expr.size() == 1) {
                Type type = funCallExpr.expr.get(0).accept(this);
                if (type == BaseType.INT) {
                    return BaseType.VOID;
                } else {
                    error("print_i()'s parameter must take a type int as an argument!");
                }
            }

        } else if (funCallExpr.string.equals("print_c")) {
            if (funCallExpr.expr.size() != 1) {
                error("print_c() should take exactly one argument!");
                return null;
            } else if (funCallExpr.expr.size() == 1) {
                Type type = funCallExpr.expr.get(0).accept(this);
                if (type == BaseType.CHAR) {
                    return BaseType.VOID;
                } else {
                    error("print_c()'s parameter must take a type char as an argument!");
                }
            }

        } else if (funCallExpr.string.equals("print_s")) {
            if (funCallExpr.expr.size() != 1) {
                error("print_s() should take exactly one argument!");
                return null;
            } else if (funCallExpr.expr.size() == 1) {
                Type type = funCallExpr.expr.get(0).accept(this);
                if (type instanceof PointerType && ((PointerType) type).type.equals(BaseType.CHAR)) {
                    return BaseType.VOID;
                } else {
                    error("print_s()'s parameter must take a type char* as an argument!");
                    return null;
                }
            }

        } else if (funCallExpr.string.equals("mcmalloc")) {
            if (funCallExpr.expr.size() != 1) {
                error("mcmalloc() should take exactly one argument!");
                return null;
            } else if (funCallExpr.expr.size() == 1) {
                Type type = funCallExpr.expr.get(0).accept(this);
                if (type == BaseType.INT) {
                    return new PointerType(BaseType.VOID);
                } else {
                    error("mcmalloc()'s parameter must take a type char* as an argument!");
                    return null;
                }
            }


        } else if (funCallExpr.fd != null) {
            if (funCallExpr.fd.params.size() == funCallExpr.expr.size()) {

                for (int i = 0; i < funCallExpr.expr.size(); i++) {
                    if (!compareTypes(funCallExpr.fd.params.get(i).type, funCallExpr.expr.get(i).accept(this))) {
                        error("Expected types do not match for parameters in function " + funCallExpr.string);
                        return null;
                    }
                }
                // No errors, expected parameters are correct, return function type
                return funCallExpr.fd.type;
            } else {
                error(funCallExpr.string + " function call expects " + funCallExpr.fd.params.size() + " parameters");
            }
        }

        return null;
    }

    @Override
    public Type visitIntLiteral(IntLiteral intLiteral) {
        return BaseType.INT;
    }

    @Override
    public Type visitChrLiteral(ChrLiteral chrLiteral) {
        return BaseType.CHAR;
    }

    @Override
    public Type visitStrLiteral(StrLiteral strLiteral) {
        int arraySize = strLiteral.string.length() + 1;
        return new ArrayType(BaseType.CHAR, arraySize);
    }

    @Override
    public Type visitArrayType(ArrayType arrayType) {

        return arrayType;
    }

    @Override
    public Type visitPointerType(PointerType pointerType) {

        return pointerType;
    }

    // To be completed...


}
