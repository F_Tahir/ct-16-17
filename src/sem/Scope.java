package sem;

import java.util.HashMap;
import java.util.Map;

public class Scope {
	private Scope outer;
	private Map<String, Symbol> symbolTable =  new HashMap<String,Symbol>();
	
	public Scope(Scope outer) { 
		this.outer = outer; 
	}
	
	public Scope() { this(null); }

	public Symbol lookup(String name) {
		Symbol symbol = lookupCurrent(name);

		// Symbol is not in current scope
		if (symbol == null) {
			
			// Symbol is not in outer scope
			if (outer == null) {
				return null;
			}
			
			// Symbol is not in current scope, but is in outer scope
			symbol = this.outer.lookup(name);
		}
		return symbol;
	}


	// This method looks into the symbol data structure only for current scope
	public Symbol lookupCurrent(String name) {
		Symbol symbol = symbolTable.get(name);
		if (symbol == null) {
			return null;
		}
		return symbol;
	}
	
	public void put(Symbol sym) {
		symbolTable.put(sym.name, sym);
	}
}
