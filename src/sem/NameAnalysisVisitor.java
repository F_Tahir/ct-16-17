package sem;


import java.util.ArrayList;
import java.util.List;
import ast.*;

/* TODO: Check all of these, wrote in a rush! Importantly, check correct usage of lookup vs
lookupCurrent as still don't completely understand when to use what.

*/


public class NameAnalysisVisitor extends BaseSemanticVisitor<Void> {
	Scope scope = new Scope();
	List<String> listOfStructs = new ArrayList<String>();
	private boolean isFuncDecl = false;



	/* This function adds the 6 predefined functions into the symbol table. This is done
	as soon as visitProgram is called (it is only visited once, at the start, so is the best way
	to include it).
	 */
	public void addBuiltInFunctions() {

		// print_s - void print_s(char* s). Briefly commented, the rest work similarly.

		// Create an array list to store function parameters
		List<VarDecl> print_s_params = new ArrayList<VarDecl>();
		// Add function parameters to the newly created array list
		print_s_params.add(new VarDecl(new PointerType(BaseType.CHAR), "s"));
		// Create a new function, passing in function type, name, parameters, and a null block
		FunDecl print_s_func = new FunDecl(BaseType.VOID, "print_s", print_s_params,  null);
		// Create a new symbol from this FunDecl
		Symbol print_s = new FuncSymbol(print_s_func);
		// Add to symbol table
		scope.put(print_s);

		// print_i - void print_i(int i);
		List<VarDecl> print_i_params = new ArrayList<VarDecl>();
		print_i_params.add(new VarDecl(BaseType.INT, "i"));
		FunDecl print_i_func = new FunDecl(BaseType.VOID, "print_i", print_i_params,  null);
		Symbol print_i = new FuncSymbol(print_i_func);
		scope.put(print_i);

		// print_c - void print_c(char c).
		List<VarDecl> print_c_params = new ArrayList<VarDecl>();
		print_c_params.add(new VarDecl(BaseType.CHAR, "c"));
		FunDecl print_c_func = new FunDecl(BaseType.VOID, "print_c", print_c_params,  null);
		Symbol print_c = new FuncSymbol(print_c_func);
		scope.put(print_c);

		// read_c - char read_c();. Pass in an empty array list
		List<VarDecl> read_c_params = new ArrayList<VarDecl>();
		FunDecl read_c_func = new FunDecl(BaseType.CHAR, "read_c", read_c_params,  null);
		Symbol read_c = new FuncSymbol(read_c_func);
		scope.put(read_c);


		// read_i - int read_i();
		List<VarDecl> read_i_params = new ArrayList<VarDecl>();
		FunDecl read_i_func = new FunDecl(BaseType.INT, "read_i", read_i_params,  null);
		Symbol read_i = new FuncSymbol(read_i_func);
		scope.put(read_i);

		// malloc - void* mcmalloc(int size);
		List<VarDecl> malloc_params = new ArrayList<VarDecl>();
		malloc_params.add(new VarDecl(BaseType.INT, "size"));
		FunDecl malloc_func = new FunDecl(new PointerType(BaseType.VOID), "mcmalloc", malloc_params, null);
		Symbol malloc = new FuncSymbol(malloc_func);
		scope.put(malloc);
	}

	@Override
	public Void visitBaseType(BaseType bt) {
		// Nothing to do here for name analysis
		return null;
	}

	@Override
	public Void visitStructType(StructType st) {

		/* Two separate cases - case one is where this is a struct type (so vars
		is empty. Case 2 is where this is a struct decl, so vars is not empty.
		For case 1, the struct has to be declared before accessing it, and for case 2, each
		struct declaration must contain a unique identifier. */

		boolean isStructDecl = st.vars.size() > 0;

		if (isStructDecl) {

			// Already declared as a struct, so declaring again results in error
			if (listOfStructs.contains(st.string)) {
				error(st.string + " declared as a struct already!");

				// Not declared, so struct declaration is fine,
			} else {
				listOfStructs.add(st.string);


				Scope oldScope = scope;
				scope = new Scope(this.scope);
				// now check contents within struct to ensure
				// no variables are being declared more than once
				for (VarDecl v : st.vars) {

					v.accept(this);
				}
				scope = oldScope;
			}

			return null;

			// Struct type e.g. struct a b; then need to ensure that a is declared as a struct
		} else {

			// Struct has not been declared, so cannot declare a type using the struct
			if (!listOfStructs.contains(st.string)) {
				error(st.string + " must be declared as a struct before using it in a struct type");
			}
		}

		return null;
	}

	@Override
	public Void visitBlock(Block b) {
		Scope oldScope = scope;

		// Create the new scope ONLY if it's not a FunDecl. If it is a FunDecl, it was created
		// in FunDecl, so is not required here. This is because the function params and the
		// function block must be in the same code.
		if (!isFuncDecl) {
			scope = new Scope(scope);
		}
		isFuncDecl = false;

		// Loop through the list of stmts and params in a block
		for (VarDecl p : b.params) {
			p.accept(this);
		}

		for (Stmt s : b.stmts) {
			s.accept(this);
		}

		scope = oldScope;
		return null;
	}

	@Override
	public Void visitFunDecl(FunDecl p) {

		scope.put(new FuncSymbol(p));
		Scope oldScope = scope;

		// Create a new scope for the function to store parameters and variable declarations. This scope
		// should be created before accepting block, so that the function declaration's parameters and
		// variable declarations within the function are in the same scope
		scope = new Scope(this.scope);

		for (VarDecl vd : p.params) {
			vd.accept(this);
		}


		// Set this to true, and create a new scope in block only if this is false (i.e. for
		// if/while statements that require a new local scope).
		isFuncDecl = true;

		p.block.accept(this);
		scope = oldScope;
		return null;

	}


	@Override
	public Void visitProgram(Program p) {

		addBuiltInFunctions();
		// TODO: Check this, most likely wrong.
		for (StructType st : p.structTypes) {
			st.accept(this);
		}

		for (VarDecl vd : p.varDecls) {
			vd.accept(this);
		}

		for (FunDecl fd : p.funDecls) {
			if (scope.lookup(fd.name) != null) {
				error(fd.name + " cannot be declared as a function. This could be because " + fd.name + " " +
						"is already declared as a global variable, or is used as another function name.");
			}
			fd.accept(this);
		}

		return null;
	}

	@Override
	public Void visitVarDecl(VarDecl vd) {

		// Look up current symbol (by identifier name) in the data structure
		// TODO: Check if this should be lookup or lookupCurrent

		Symbol s = scope.lookupCurrent(vd.varName);

		// Variable already declared in scope, so declaring again results in error
		if (s != null) {
			error(vd.varName + " declared twice!");

			// Not declared, so passes name analysis, store it.
		} else {
			// Checks that if it is a struct decl, then check if the Struct has been declared.
			vd.type.accept(this);
			scope.put(new VarSymbol(vd));
		}

		return null;
	}

	@Override
	public Void visitVarExpr(VarExpr v) {
		Symbol vs = scope.lookup(v.name);
		if (vs == null) {
			error(v.name + " was called as a variable, before being declared!");
		} else if (!vs.isVar()) {
			error(v.name + " was called as a variable, but is not a variable!");
			// Everything is fine, record VarDecl
		} else {
			v.vd = ((VarSymbol) vs).vd;
		}
		return null;
	}

	@Override
	public Void visitReturn(Return rt) {
		// Return expression is optional, so check
		// if it's not null before accepting, otherwise
		// it will throw null pointer exception
		if (rt.expr != null) {
			rt.expr.accept(this);
		}
		return null;
	}

	@Override
	public Void visitAssign(Assign assign) {
		assign.expr1.accept(this);
		assign.expr2.accept(this);
		return null;
	}

	@Override
	public Void visitIf(If if1) {
		if1.expr.accept(this);
		if1.stmt1.accept(this);

		// Stmt2 is optional, so only accept if not null,
		// otherwise will throw a null pointer exception
		if (if1.stmt2 != null) {
			if1.stmt2.accept(this);
		}

		return null;
	}

	@Override
	public Void visitWhile(While while1) {
		while1.expr.accept(this);
		while1.stmt.accept(this);
		return null;
	}

	@Override
	public Void visitExprStmt(ExprStmt exprStmt) {
		exprStmt.expr.accept(this);
		return null;
	}

	@Override
	public Void visitTypecastExpr(TypecastExpr typecastExpr) {
		// Do not need to visit type, this is part of typechecking
		typecastExpr.expr.accept(this);
		return null;
	}

	@Override
	public Void visitSizeOfExpr(SizeOfExpr sizeOfExpr) {
		sizeOfExpr.type.accept(this);
		return null;
	}

	@Override
	public Void visitValueAtExpr(ValueAtExpr valueAtExpr) {
		valueAtExpr.expr.accept(this);
		return null;
	}

	@Override
	public Void visitFieldAccessExpr(FieldAccessExpr fieldAccessExpr) {
		fieldAccessExpr.expr.accept(this);

		return null;
	}

	@Override
	public Void visitArrayAccessExpr(ArrayAccessExpr arrayAccessExpr) {
		// Check if identifier is declared before being accessed, e.g. a[20] should fail
		// if a wasn't declared
		arrayAccessExpr.expr1.accept(this);
		arrayAccessExpr.expr2.accept(this);
		return null;
	}

	@Override
	public Void visitBinOp(BinOp binOp) {

		// Do not need to visit the operator, as no name
		// analysis needs to be done
		binOp.expr1.accept(this);
		binOp.expr2.accept(this);
		return null;
	}

	@Override
	public Void visitFunCallExpr(FunCallExpr funCallExpr) {

		Symbol func = scope.lookup(funCallExpr.string);

		// Undeclared function called
		if (func == null) {
			error(funCallExpr.string + " called, but not declared!");

			// A non function was called as a function
		} else if (!func.isFunc()) {
			error("The function " + funCallExpr.string + " was called, but is not a function in the local scope!");

			// Funcall declared and is a function, so check parameters
		} else {
			funCallExpr.fd = ((FuncSymbol) func).p;
		}


		for (Expr fc : funCallExpr.expr) {
			fc.accept(this);
		}

		return null;
	}

	@Override
	public Void visitIntLiteral(IntLiteral intLiteral) {
		// Nothing to do with here regarding name analysis
		return null;
	}

	@Override
	public Void visitChrLiteral(ChrLiteral chrLiteral) {
		// Nothing to do with here regarding name analysis
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral strLiteral) {
		// Nothing to do with here regarding name analysis
		return null;
	}

	@Override
	public Void visitArrayType(ArrayType arrayType) {
		// Nothing to do with here regarding name analysis
		return null;
	}

	@Override
	public Void visitPointerType(PointerType pointerType) {
		// Nothing to do with here regarding name analysis
		return null;
	}


}