 package parser;

import ast.*;
import lexer.Token;
import lexer.Tokeniser;
import lexer.Token.TokenClass;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static lexer.Token.TokenClass.SC;

 public class Parser {

    private Token token;

    // use for backtracking (useful for distinguishing decls from procs when parsing a program for instance)
    private Queue<Token> buffer = new LinkedList<Token>();

    private final Tokeniser tokeniser;



    public Parser(Tokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }

    public Program parse() {
   	// get the first token
    	nextToken();

        return parseProgram();
   }




    public int getErrorCount() {
        return error;
    }

    private int error = 0;
    private Token lastErrorToken;

    private void error(TokenClass... expected) {

        if (lastErrorToken == token) {
            // skip this error, same token causing trouble
            return;
        }

        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (TokenClass e : expected) {
            sb.append(sep);
            sb.append(e);
            sep = "|";
        }
        System.out.println("Parsing error: expected ("+sb+") found ("+token+") at "+token.position);

        error++;
        lastErrorToken = token;
    }

    /*
     * Look ahead the i^th element from the stream of token.
     * i should be >= 1
     */
    private Token lookAhead(int i) {
        // ensures the buffer has the element we want to look ahead
        while (buffer.size() < i)
            buffer.add(tokeniser.nextToken());
        assert buffer.size() >= i;

        int cnt=1;
        for (Token t : buffer) {
            if (cnt == i)
                return t;
            cnt++;
        }

        assert false; // should never reach this
        return null;
    }


    /*
     * Consumes the next token from the tokeniser or the buffer if not empty.
     */
    private void nextToken() {
        if (!buffer.isEmpty())
            token = buffer.remove();
        else
            token = tokeniser.nextToken();
    }

    /*
     * If the current token is equals to the expected one, then skip it, otherwise report an error.
     * Returns the expected token or null if an error occurred.
     */
    private Token expect(TokenClass... expected) {
        for (TokenClass e : expected) {
            if (e == token.tokenClass) {
                Token cur = token;
                nextToken();
                return cur;
            }
        }

        error(expected);
        return null;
    }

    /*
    * Returns true if the current token is equals to any of the expected ones.
    */
    private boolean accept(TokenClass... expected) {
        boolean result = false;
        for (TokenClass e : expected)
            result |= (e == token.tokenClass);
        return result;
    }



    /* Helper functions to make non-terminal methods shorter */
    private boolean hasType() {
 	   if (accept(TokenClass.CHAR, TokenClass.INT, TokenClass.VOID)) {
 		   return true;
 	   }
 	   return false;
    }

    private boolean hasStruct() {
    	if (accept(TokenClass.STRUCT)) {
    		return true;
    	}
    	return false;
    }

    private boolean hasExp() {
 	   if (accept(TokenClass.LPAR, TokenClass.MINUS, TokenClass.IDENTIFIER,
 			   TokenClass.INT_LITERAL, TokenClass.CHAR_LITERAL,
 			   TokenClass.STRING_LITERAL, TokenClass.ASTERIX, TokenClass.SIZEOF))  {
 		   return true;
 	   }
 	   return false;
    }

    private Op getOp(Token token) {

    	// Possibly add error checking to make sure token is not null
    	if (token == null) {
    		return null;
    	}

    	switch(token.tokenClass) {
    		case AND: return Op.AND;
    		case OR: return Op.OR;
    		case EQ: return Op.EQ;
    		case NE: return Op.NE;
    		case LT: return Op.LT;
    		case GT: return Op.GT;
    		case GE: return Op.GE;
    		case LE: return Op.LE;
    		case PLUS: return Op.ADD;
    		case MINUS: return Op.SUB;
    		case ASTERIX: return Op.MUL;
    		case DIV: return Op.DIV;
    		case REM: return Op.MOD;

    		// Should not reach this case
    		default: return null;
    	}
    }


    private Program parseProgram() {
        parseIncludesKleene();
        List<StructType> sts = parseStructDeclsKleene();
        List<VarDecl> vds = parseVarDeclsKleene();
        List<FunDecl> fds = parseFunDeclsKleene();
        expect(TokenClass.EOF);
        return new Program(sts, vds, fds);
    }


    private void parseIncludesKleene() {
    	if (accept(TokenClass.INCLUDE) &&
    			lookAhead(1).tokenClass == TokenClass.STRING_LITERAL) {
    		parseIncludes();
    		parseIncludesKleene();
    	}
    }

    // includes are ignored, so does not need to return an AST node
    private void parseIncludes() {
	    expect(TokenClass.INCLUDE);
	    expect(TokenClass.STRING_LITERAL);
    }

    //TODO
    private List<StructType> parseStructDeclsKleene() {

    	List<StructType> result = new ArrayList<StructType>();
    	List<StructType> moreStructDecls = new ArrayList<StructType>();

    	if (accept(TokenClass.STRUCT) &&
	    		lookAhead(1).tokenClass == TokenClass.IDENTIFIER
	    		&& lookAhead(2).tokenClass == TokenClass.LBRA) {
	    	StructType structDecl = parseStructDecl();

	    	result.add(structDecl);

	    	moreStructDecls.addAll(parseStructDeclsKleene());
	    	result.addAll(moreStructDecls);

	    }
	return result;
    }

    private List<VarDecl> parseVarDeclsKleene() {
    	List<VarDecl> result = new ArrayList<VarDecl>();
    	List<VarDecl> nextDeclaration = new ArrayList<VarDecl>();

    	// First checks for "int IDENT; or int IDENT["
    	// Second checks for "int* IDENT; or int* IDENT["
    	// Third checks for "struct a b or struct a b["
    	// Fourth checks for "struct a* b or struct a* b["
    	if ( (hasType() && lookAhead(1).tokenClass == TokenClass.IDENTIFIER &&
    			(lookAhead(2).tokenClass == SC || lookAhead(2).tokenClass == TokenClass.LSBR))

    			|| (hasType() && lookAhead(1).tokenClass == TokenClass.ASTERIX
    			&& lookAhead(2).tokenClass == TokenClass.IDENTIFIER
    			&& (lookAhead(3).tokenClass == SC || lookAhead(3).tokenClass == TokenClass.LSBR))

    			|| (hasStruct() && lookAhead(1).tokenClass == TokenClass.IDENTIFIER &&
    			lookAhead(2).tokenClass == TokenClass.IDENTIFIER &&
    			(lookAhead(3).tokenClass == SC || lookAhead(3).tokenClass == TokenClass.LSBR))

    			|| (hasStruct() && lookAhead(1).tokenClass == TokenClass.IDENTIFIER &&
    			lookAhead(2).tokenClass == TokenClass.ASTERIX && lookAhead(3).tokenClass == TokenClass.IDENTIFIER) &&
    			(lookAhead(4).tokenClass == SC || lookAhead(4).tokenClass == TokenClass.LSBR)) {

    		VarDecl varDecl = parseVarDecl();
    		result.add(varDecl);

    		nextDeclaration.addAll(parseVarDeclsKleene());
    		result.addAll(nextDeclaration);

    	}
	return result;
    }

    // First if condition checks for "int a("
    // Second checks for "int* a("
    // Third checks for "struct a b("
    // Fourth checks for "struct a* b("
    private List<FunDecl> parseFunDeclsKleene() {

    	List<FunDecl> funDecls = new ArrayList<FunDecl>();
    	List<FunDecl> moreFunDecls = new ArrayList<FunDecl>();

    	if( (hasType() && lookAhead(1).tokenClass == TokenClass.IDENTIFIER
    			&& lookAhead(2).tokenClass == TokenClass.LPAR)

    		|| (hasType() && lookAhead(1).tokenClass == TokenClass.ASTERIX &&
    		lookAhead(2).tokenClass == TokenClass.IDENTIFIER &&
    		lookAhead(3).tokenClass == TokenClass.LPAR)

    		|| (hasStruct() && lookAhead(1).tokenClass == TokenClass.IDENTIFIER &&
    		lookAhead(2).tokenClass == TokenClass.IDENTIFIER &&
    		lookAhead(3).tokenClass == TokenClass.LPAR)

    		|| (hasStruct() && lookAhead(1).tokenClass == TokenClass.IDENTIFIER &&
    		lookAhead(2).tokenClass == TokenClass.ASTERIX &&
    		lookAhead(3).tokenClass == TokenClass.IDENTIFIER &&
    		lookAhead(4).tokenClass == TokenClass.LPAR) ) {


    		FunDecl singleFunDecl = parseFunDecl();
    		funDecls.add(singleFunDecl);

    		moreFunDecls.addAll(parseFunDeclsKleene());
    		funDecls.addAll(moreFunDecls);
    	}
	return funDecls;
   }


    private StructType parseStructDecl() {
		String string = parseStructType();

    	expect(TokenClass.LBRA);
    	List<VarDecl> varDecls = parseVarDeclPos();

    	expect(TokenClass.RBRA);
    	expect(SC);

    	return new StructType(string, varDecls);
    }


    private List<VarDecl> parseVarDeclPos() {

		List<VarDecl> varDeclList = new ArrayList<VarDecl>();
    	List<VarDecl> moreVars = new ArrayList<VarDecl>();

    	VarDecl varDecl = parseVarDecl();
    	varDeclList.add(varDecl);

		// Check if we have a format such as int a; or struct a b;
    	if ( hasType() || hasStruct()) {

			moreVars.addAll(parseVarDeclPos());
    		varDeclList.addAll(moreVars);
    	}

    	return varDeclList;
    }


    private String parseStructType() {
    	expect(TokenClass.STRUCT);
    	Token expectedToken = expect(TokenClass.IDENTIFIER);
 	    String identifier = expectedToken == null ? null : expectedToken.data;

    	return identifier;
    }


   private VarDecl parseVarDecl() {

	   Type type = parseType();
	   Token expectedToken = expect(TokenClass.IDENTIFIER);
	   String identifier = expectedToken == null ? null : expectedToken.data;

	   if (accept(SC)) {
		   nextToken();
		   return new VarDecl(type, identifier);
	   } else {
		   expect(TokenClass.LSBR);

		   Token intToken = expect(TokenClass.INT_LITERAL);
		   String arrayIndex = intToken == null ? null : intToken.data;
		   expect(TokenClass.RSBR);
		   expect(SC);


		   // Integer.parseInt() will throw an error if I don't check if arrayIndex is not null
		   if (arrayIndex != null) {
			   return new VarDecl(new ArrayType(type, Integer.parseInt(arrayIndex)), identifier);
		   }
	   }

	   // Shouldn't be reached.
	   return null;

   }


   private Type parseType() {

	   String varType = token.tokenClass.toString();
	   List<VarDecl> emptyVarList = new ArrayList<VarDecl>();

	   if (hasType()) {
		   nextToken();
		   boolean result = parseAsterixOpt();
		   if (result) {
			   return new PointerType(BaseType.valueOf(varType));
		   } else {
			   return BaseType.valueOf(varType);
		   }


	   } else if (hasStruct()){
		   String type = parseStructType();
		   boolean result = parseAsterixOpt();
		   if (result) {
			   return new PointerType(new StructType(type, emptyVarList));
		   } else {
			   return new StructType(type, emptyVarList);
		   }

	   }

	   return null;
   }

   private boolean parseAsterixOpt() {
	   if (accept(TokenClass.ASTERIX)) {
		   nextToken();
		   return true;
	   }
	   return false;
   }


   private FunDecl parseFunDecl() {
	   Type type = parseType();
	   Token expectedToken = expect(TokenClass.IDENTIFIER);
	   String identifier = expectedToken == null ? null : expectedToken.data;

	   expect(TokenClass.LPAR);
	   List<VarDecl> params = parseParams();
	   expect(TokenClass.RPAR);
	   Block block = parseBlock();

	   return new FunDecl(type, identifier, params, block);
   }

   private List<VarDecl> parseParams() {
	   return parseParamsOpt();
   }

   private List<VarDecl> parseParamsOpt() {

	   List<VarDecl> result = new ArrayList<VarDecl>();
	   List<VarDecl> moreVarDecls = new ArrayList<VarDecl>();

	   if (hasType() || hasStruct()) {

		   Type type = parseType();
		   Token expectedToken = expect(TokenClass.IDENTIFIER);

		   String identifier = expectedToken == null ? null : expectedToken.data;

		   result.add(new VarDecl(type, identifier));

		   moreVarDecls.addAll(parseTypeIdentKleene());
		   result.addAll(moreVarDecls);
	   }

	   return result;
   }

   private List<VarDecl> parseTypeIdentKleene() {

	   List<VarDecl> result = new ArrayList<VarDecl>();
	   List<VarDecl> additionalVarDecls = new ArrayList<VarDecl>();
	   Type type;
	   String identifier;

	   if(accept(TokenClass.COMMA)) {
		   nextToken();
		   type = parseType();
		   Token expectedToken = expect(TokenClass.IDENTIFIER);
		   identifier = expectedToken == null ? null : expectedToken.data;

		   VarDecl varDecl = new VarDecl(type, identifier);
		   result.add(varDecl);

		   additionalVarDecls.addAll(parseTypeIdentKleene());
		   result.addAll(additionalVarDecls);

	   }

	   return result;
   }


   private Stmt parseStmt() {
	  		// block
	  if (accept(TokenClass.LBRA)) {
		   return parseBlock();

		   // "while" "(" exp ")" stmt
	  } else if (accept(TokenClass.WHILE)) {
		   nextToken();
		   expect(TokenClass.LPAR);
		   Expr exp = parseExp();
		   expect(TokenClass.RPAR);
		   Stmt stmtWhile = parseStmt();

		   return new While(exp, stmtWhile);

		   // "if" "(" exp ")" stmt elsestmtopt
	  } else if (accept(TokenClass.IF)) {
		   nextToken();
		   expect(TokenClass.LPAR);
		   Expr exp = parseExp();
		   expect(TokenClass.RPAR);

		   Stmt stmtIf = parseStmt();
		   Stmt stmtElse = parseElseStmtOpt();

		   return new If(exp, stmtIf, stmtElse);

		   // "return" expopt ";"
	  } else if (accept(TokenClass.RETURN)) {
		   nextToken();
		   Expr exp = parseExpOpt();
		   expect(SC);

		   return new Return(exp);

		   // exp "=" exp ";" or exp "
	  } else if (hasExp()) {
		  Expr lhs = parseExp();

		  if (accept(TokenClass.ASSIGN)) {

			  nextToken();
			  Expr rhs = parseExp();
			  expect(SC);
			  return new Assign(lhs, rhs);
		  } else {
			  expect(SC);
			  return new ExprStmt(lhs);
		  }
	  }
	  return null;
   }

   private Stmt parseElseStmtOpt() {

	   Stmt stmt = null;
	   if (accept(TokenClass.ELSE)) {
		   nextToken();
		   stmt = parseStmt();
	   }
	   return stmt;
   }


   private Expr parseExpOpt() {
	   if (hasExp()) {
		   return parseExp();
	   }
	   return null;
   }

   private Block parseBlock() {
	   List<VarDecl> varDeclList = new ArrayList<VarDecl>();
	   List<Stmt> stmts = new ArrayList<Stmt>();
	   expect(TokenClass.LBRA);

	   varDeclList.addAll(parseVarDeclsKleene());
	   stmts.addAll(parseStmtKleene());


	   expect(TokenClass.RBRA);

	   return new Block(varDeclList, stmts);
   }

   // Can start with "while", "if", "return" or "{" from block,
   // or any expression
   private List<Stmt> parseStmtKleene() {

	   List<Stmt> stmt = new ArrayList<Stmt>();
	   List<Stmt> moreStmts = new ArrayList<Stmt>();

	   if (accept(TokenClass.LBRA, TokenClass.WHILE,
			   TokenClass.IF, TokenClass.RETURN) || hasExp()) {
		   Stmt singleStmt = parseStmt();
		   stmt.add(singleStmt);

		   moreStmts.addAll(parseStmtKleene());
		   stmt.addAll(moreStmts);

	   }
	   return stmt;
   }

   private Expr parseExp() {
	   Expr lhs = parseExp1();
	   while (accept(TokenClass.OR)) {
		   nextToken();
		   Expr moreLhs = parseExp1();
		   lhs = new BinOp(lhs, Op.OR, moreLhs);
	   }
	   return lhs;
   }

   private Expr parseExp1() {
	   Expr lhs = parseExp2();
	   while (accept(TokenClass.AND)) {
		   nextToken();
		   Expr moreLhs = parseExp2();
		   lhs = new BinOp(lhs, Op.AND, moreLhs);
	   }
	   return lhs;
   }

   private Expr parseExp2() {
	   Expr lhs = parseExp3();
	   while (accept(TokenClass.EQ, TokenClass.NE)) {
		   Op op = getOp(token);
		   nextToken();
		   Expr moreLhs = parseExp3();
		   lhs = new BinOp(lhs, op, moreLhs);
	   }
	   return lhs;
   }

   private Expr parseExp3() {
	   Expr lhs = parseExp4();	   
	   while(accept(TokenClass.LT, TokenClass.LE, TokenClass.GT, TokenClass.GE)) {
		   Op op = getOp(token);
		   nextToken();
		   Expr moreLhs = parseExp4();
		   lhs = new BinOp(lhs, op, moreLhs);
				   
	   }
	   return lhs;
   }

   // ADD, MINUS, DIV, REM, ASTERIX should all be left assosciative
   // Should equality be left associative also?
   private Expr parseExp4() {
	   Expr lhs = parseExp5();

	   while (accept(TokenClass.PLUS, TokenClass.MINUS)) {
		   Op op = getOp(token);
		   nextToken();
		   Expr moreLhs = parseExp5();
		   lhs = new BinOp(lhs, op, moreLhs);
	   }
	   return lhs;
   }

   private Expr parseExp5() {
	   Expr lhs = parseExp6();

	   while (accept(TokenClass.DIV, TokenClass.REM, TokenClass.ASTERIX)) {
		   Op op = getOp(token);
		   nextToken();
		   Expr moreLhs = parseExp6();
		   lhs = new BinOp(lhs, op, moreLhs);
	   }
	   return lhs;
   }

   // Pointer - allows for multiple pointers
   private Expr parseExp6() {
	   if (accept(TokenClass.ASTERIX)) {
		   nextToken();

		   // Possibly change this to parseExp6();
		   Expr exp = parseExp6();
		   return new ValueAtExpr(exp);

	   } else {
		   return parseExp7();
	   }
   }


   // Sizeof - grammar only allows one sizeof(), so no recursive call
   private Expr parseExp7() {
	   if (accept(TokenClass.SIZEOF)) {
		   expect(TokenClass.SIZEOF);
		   expect(TokenClass.LPAR);
		   Type type = parseType();
		   expect(TokenClass.RPAR);

		   return new SizeOfExpr(type);

	   } else {
		   return parseExp8();
	   }
   }

   // Typecast
   private Expr parseExp8() {
	   if (accept(TokenClass.LPAR) &&
			   lookAhead(1).tokenClass == TokenClass.INT ||
			   lookAhead(1).tokenClass == TokenClass.VOID ||
			   lookAhead(1).tokenClass == TokenClass.CHAR ||
			   lookAhead(1).tokenClass == TokenClass.STRUCT ) {


		   expect(TokenClass.LPAR);
		   Type type = parseType();
		   expect(TokenClass.RPAR);
		   Expr exp = parseExp();
		   return new TypecastExpr(type, exp);
	   } else {
		   return parseExp9();
	   }
   }

	// Field Access
   private Expr parseExp9() {
	   Expr exp = parseExp10();
	   return parseFieldAccess(exp);
   }



   // Array Access
   private Expr parseExp10() {
	   Expr exp = parseExp11();
	   return parseArrayAccess(exp);

   }

   private Expr parseExp11() {
	   // Function call
	   if (accept(TokenClass.IDENTIFIER) &&
			   lookAhead(1).tokenClass == TokenClass.LPAR ) {

		   Token expectedToken = expect(TokenClass.IDENTIFIER);
		   String identifier = expectedToken == null ? null : expectedToken.data;

		   expect(TokenClass.LPAR);
		   List<Expr> exprs = new ArrayList<Expr>();
		   List<Expr> moreExprs = parseExpOptional();

		   exprs.addAll(moreExprs);



		   expect(TokenClass.RPAR);
		   return new FunCallExpr(identifier, exprs);

		// This is for putting parentheses around expressions, and can get confused
		 // with typecasting, so look ahead and ensure it's not a type.
	   } else if (accept(TokenClass.LPAR) &&
			   (lookAhead(1).tokenClass != TokenClass.INT
			   && lookAhead(1).tokenClass != TokenClass.CHAR
			   && lookAhead(1).tokenClass != TokenClass.VOID
			   && lookAhead(1).tokenClass != TokenClass.STRUCT) ) {

		   nextToken();
		   Expr exp = parseExp();
		   expect(TokenClass.RPAR);
		   return exp;

	   } else if (accept(TokenClass.MINUS)) {
		   nextToken();

		   Expr exp = parseExp11();


		   return new BinOp(new IntLiteral(0), Op.SUB, exp);


	   /* No default else condition, because if for example the current token is sizeof
		and the default else expects identifier, char_lit, string, or int,
		an error will be thrown because sizeof does not match, even though it will be
		matched in parseExpr7(), after traversing all the way down to parseExpr11.
		(after traversing down here, it traverses back up all the methods
		to do a check on the if statements. */
	   } else if (accept(TokenClass.IDENTIFIER)) {

		   Token expectedToken = expect(TokenClass.IDENTIFIER);
		   String identifier = expectedToken == null ? null : expectedToken.data;
		   return new VarExpr(identifier);


	   } else if (accept(TokenClass.CHAR_LITERAL)) {

		   Token expectedToken = expect(TokenClass.CHAR_LITERAL);
		   String identifier = expectedToken == null ? null : expectedToken.data;
		   return new ChrLiteral(identifier.charAt(0));

	   } else if (accept(TokenClass.STRING_LITERAL)) {
		   Token expectedToken = expect(TokenClass.STRING_LITERAL);
		   String identifier = expectedToken == null ? null : expectedToken.data;
		   return new StrLiteral(identifier);

	   } else if (accept(TokenClass.INT_LITERAL)) {
		   Token expectedToken = expect(TokenClass.INT_LITERAL);
		   String identifier = expectedToken == null ? null : expectedToken.data;

		   return new IntLiteral(Integer.parseInt(identifier));
	   } else {
		   error();
	   }

	   System.out.println("Shouldn't reach this null");
	   return null;


   }

   private Expr parseFieldAccess(Expr exp) {

	   String identifier;

	   // Recursive call to check something like foo.bar.x
	   while (accept(TokenClass.DOT)) {
		   nextToken();
		   Token expectedToken = expect(TokenClass.IDENTIFIER);
		   identifier = expectedToken == null ? null : expectedToken.data;


		   exp = new FieldAccessExpr(exp, identifier);
	   }
	   return exp;
   }

   private Expr parseArrayAccess(Expr exp) {


	   // Recursive call to check multidimensional arrays e.g. a[3][4][5]
	   while (accept(TokenClass.LSBR)) {
		   nextToken();
		   Expr index = parseExp();
		   exp = new ArrayAccessExpr(exp, index);
		   expect(TokenClass.RSBR);
	   }
	   return exp;
   }


   // Naming confliction - this method is used to remove the optional
   // from FunCall in the original EBNF grammar
   private List<Expr> parseExpOptional() {

	List<Expr> exprs = new ArrayList<Expr>();
	List<Expr> moreExprs = new ArrayList<Expr>();

	if (hasExp()) {
		exprs.add(parseExp());

		moreExprs.addAll(parseExpCommaKleene());
		exprs.addAll(moreExprs);

	}
	return exprs;
   }

   private List<Expr> parseExpCommaKleene() {
	   List<Expr> exprs = new ArrayList<Expr>();
	   List<Expr> moreExprs = new ArrayList<Expr>();
	   if (accept(TokenClass.COMMA)) {
		   nextToken();
		   exprs.add(parseExp());

		   moreExprs.addAll(parseExpCommaKleene());

		   exprs.addAll(moreExprs);

	   }
	   return exprs;
   }


}
