# Install script for directory: /home/faizan/Documents/Projects/ct-16-17/llvm/tools/clang

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "MinSizeRel")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES
    "/home/faizan/Documents/Projects/ct-16-17/llvm/tools/clang/include/clang"
    "/home/faizan/Documents/Projects/ct-16-17/llvm/tools/clang/include/clang-c"
    FILES_MATCHING REGEX "/[^/]*\\.def$" REGEX "/[^/]*\\.h$" REGEX "/config\\.h$" EXCLUDE REGEX "/\\.svn$" EXCLUDE)
endif()

if(NOT CMAKE_INSTALL_COMPONENT OR "${CMAKE_INSTALL_COMPONENT}" STREQUAL "Unspecified")
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include" TYPE DIRECTORY FILES "/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/include/clang" FILES_MATCHING REGEX "/CMakeFiles$" EXCLUDE REGEX "/[^/]*\\.inc$" REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/utils/TableGen/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/include/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/lib/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/tools/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/runtime/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/unittests/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/test/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/utils/perf-training/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/docs/cmake_install.cmake")
  include("/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/cmake/modules/cmake_install.cmake")

endif()

