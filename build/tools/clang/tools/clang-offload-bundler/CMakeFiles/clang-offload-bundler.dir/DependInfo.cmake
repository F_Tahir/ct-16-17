# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/faizan/Documents/Projects/ct-16-17/llvm/tools/clang/tools/clang-offload-bundler/ClangOffloadBundler.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/tools/clang-offload-bundler/CMakeFiles/clang-offload-bundler.dir/ClangOffloadBundler.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "CLANG_ENABLE_ARCMT"
  "CLANG_ENABLE_OBJC_REWRITER"
  "CLANG_ENABLE_STATIC_ANALYZER"
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "tools/clang/tools/clang-offload-bundler"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/tools/clang/tools/clang-offload-bundler"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/tools/clang/include"
  "tools/clang/include"
  "include"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Bitcode/Writer/CMakeFiles/LLVMBitWriter.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/tools/clang/lib/Basic/CMakeFiles/clangBasic.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Analysis/CMakeFiles/LLVMAnalysis.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/ProfileData/CMakeFiles/LLVMProfileData.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Demangle/CMakeFiles/LLVMDemangle.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
