# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/AliasAnalysisTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/AliasAnalysisTest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/BlockFrequencyInfoTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/BlockFrequencyInfoTest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/CFGTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/CFGTest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/CGSCCPassManagerTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/CGSCCPassManagerTest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/CallGraphTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/CallGraphTest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/LazyCallGraphTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/LazyCallGraphTest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/LoopPassManagerTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/LoopPassManagerTest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/ScalarEvolutionTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/ScalarEvolutionTest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/TBAATest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/TBAATest.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/UnrollAnalyzer.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/UnrollAnalyzer.cpp.o"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis/ValueTrackingTest.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/unittests/Analysis/CMakeFiles/AnalysisTests.dir/ValueTrackingTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "unittests/Analysis"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/unittests/Analysis"
  "include"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/include"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/utils/unittest/googletest/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Analysis/CMakeFiles/LLVMAnalysis.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/AsmParser/CMakeFiles/LLVMAsmParser.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/utils/unittest/UnitTestMain/CMakeFiles/gtest_main.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/utils/unittest/CMakeFiles/gtest.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/ProfileData/CMakeFiles/LLVMProfileData.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Demangle/CMakeFiles/LLVMDemangle.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
