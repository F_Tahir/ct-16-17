# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/faizan/Documents/Projects/ct-16-17/llvm/examples/Fibonacci/fibonacci.cpp" "/home/faizan/Documents/Projects/ct-16-17/build/examples/Fibonacci/CMakeFiles/Fibonacci.dir/fibonacci.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GTEST_HAS_RTTI=0"
  "_GNU_SOURCE"
  "__STDC_CONSTANT_MACROS"
  "__STDC_FORMAT_MACROS"
  "__STDC_LIMIT_MACROS"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "examples/Fibonacci"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/examples/Fibonacci"
  "include"
  "/home/faizan/Documents/Projects/ct-16-17/llvm/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/IR/CMakeFiles/LLVMCore.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/ExecutionEngine/CMakeFiles/LLVMExecutionEngine.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/ExecutionEngine/Interpreter/CMakeFiles/LLVMInterpreter.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/MC/CMakeFiles/LLVMMC.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/ExecutionEngine/MCJIT/CMakeFiles/LLVMMCJIT.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Support/CMakeFiles/LLVMSupport.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Target/X86/CMakeFiles/LLVMX86CodeGen.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Target/X86/MCTargetDesc/CMakeFiles/LLVMX86Desc.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Target/X86/TargetInfo/CMakeFiles/LLVMX86Info.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/ExecutionEngine/RuntimeDyld/CMakeFiles/LLVMRuntimeDyld.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/MC/MCDisassembler/CMakeFiles/LLVMMCDisassembler.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/CodeGen/AsmPrinter/CMakeFiles/LLVMAsmPrinter.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/DebugInfo/CodeView/CMakeFiles/LLVMDebugInfoCodeView.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/DebugInfo/MSF/CMakeFiles/LLVMDebugInfoMSF.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/CodeGen/GlobalISel/CMakeFiles/LLVMGlobalISel.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/CodeGen/SelectionDAG/CMakeFiles/LLVMSelectionDAG.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/CodeGen/CMakeFiles/LLVMCodeGen.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Target/CMakeFiles/LLVMTarget.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Bitcode/Writer/CMakeFiles/LLVMBitWriter.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Transforms/Instrumentation/CMakeFiles/LLVMInstrumentation.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Transforms/Scalar/CMakeFiles/LLVMScalarOpts.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Transforms/InstCombine/CMakeFiles/LLVMInstCombine.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Transforms/Utils/CMakeFiles/LLVMTransformUtils.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Analysis/CMakeFiles/LLVMAnalysis.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Object/CMakeFiles/LLVMObject.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Bitcode/Reader/CMakeFiles/LLVMBitReader.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/MC/MCParser/CMakeFiles/LLVMMCParser.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/ProfileData/CMakeFiles/LLVMProfileData.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Target/X86/InstPrinter/CMakeFiles/LLVMX86AsmPrinter.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Target/X86/Utils/CMakeFiles/LLVMX86Utils.dir/DependInfo.cmake"
  "/home/faizan/Documents/Projects/ct-16-17/build/lib/Demangle/CMakeFiles/LLVMDemangle.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
