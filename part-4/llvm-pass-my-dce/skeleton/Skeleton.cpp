#define DEBUG_TYPE "opCounter"
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include <vector>
#include "llvm/ADT/SmallVector.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/Transforms/Scalar.h"
#include "llvm/ADT/DepthFirstIterator.h"
#include "llvm/ADT/SmallPtrSet.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/IR/BasicBlock.h"
#include "llvm/IR/CFG.h"
#include "llvm/IR/Instructions.h"
#include "llvm/IR/IntrinsicInst.h"


using namespace llvm;


void countInstructions(Function &F) {

	std::map<std::string, int> opCounter;
	for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
		for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
			if(opCounter.find(i->getOpcodeName()) == opCounter.end()) {
				opCounter[i->getOpcodeName()] = 1;
			} else {
				opCounter[i->getOpcodeName()] += 1;
			}
		}
	}

	std::map <std::string, int>::iterator i = opCounter.begin();
	std::map <std::string, int>::iterator e = opCounter.end();
	while (i != e) {
		errs() << i->first << ": " << i->second << "\n";
		i++;
	}
	opCounter.clear();
}


void eliminateDeadCode(Function &F) {

	SmallPtrSet<Instruction*, 32> Alive;
	SmallVector<Instruction*, 128> Worklist;


	// Loop through functions' instructions and get a list of instructions that
	// are live. Instructions that have no uses (such as returns), and instructions
	// that have side effects (e.g. calling a function that prints something) are
	// considered live.
	for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I) {
		Instruction *inst = &*I;

		if (isa<TerminatorInst>(inst) || isa<DbgInfoIntrinsic>(inst) || isa<LandingPadInst>(inst)
		|| inst->mayHaveSideEffects()) {

			Alive.insert(inst);
			Worklist.push_back(inst);
		}
	}

	// Loop through all the alive instruction's operands, and if these operands are,
	// ainstructions, add them to the alive set (as they are being used in the operands)
	while (!Worklist.empty()) {
	 	Instruction *Curr = Worklist.pop_back_val();
	 	for (Use &OI : Curr->operands()) {

			// Operand is also an instruction, so add it to the Alive list,
			// as it is being used
	 		if (Instruction *Inst = dyn_cast<Instruction>(OI)) {

				// Insert returns a pair, where the second tuple is a boolean stating
				// whether insertion was successful. Insertion would fail if the
				// instruction was already in the set, so this avoids us re-examining
				// an instruction we just looked at.
	 			if (Alive.insert(Inst).second) {

					// Push to the worklist so we can check if the operand instruction
					// has any operands that are also instructions (and are thus alive)
	 				Worklist.push_back(Inst);
	 			}
	 		}
	 	}
	 }



	 // Instructions not in live set are considered dead, so drop all references
	 // to that instruction, and add these instructions to the worklist, to be removed.
	 for (Instruction &I : instructions(F)) {

		 // Check how many times the instruction appears in the Alive set. IIf
		 // the instruction is truly dead, it will appear in Alive 0 times, so
		 // Alive.count(&I) = 0, and !0 = 1 = true, so add the dead instruction
		 // to worklist as the condition is true
		 if (!Alive.count(&I)) {
			 Worklist.push_back(&I);
			 I.dropAllReferences();
		 }
	 }


	 // Now we have only dead instructions in worklist, so loop through worklist
	 // and erase the instructions
	 for (Instruction *&I : Worklist) {
		 I->eraseFromParent();
	 }

}

namespace {
   struct MyDCE : public FunctionPass {
     static char ID;
     MyDCE() : FunctionPass(ID) {}


     virtual bool runOnFunction(Function &F) {
       errs() << "BEFORE DCE\n";
       countInstructions(F);
       errs() << "DCE START\n";
       eliminateDeadCode(F);
       countInstructions(F);
       //Eliminate dead code and produce counts here
       errs() << "DCE END\n";
       return false;
     }
   };
}
char MyDCE::ID = 0;
static RegisterPass<MyDCE> X("mydce", "My dead code elimination");
