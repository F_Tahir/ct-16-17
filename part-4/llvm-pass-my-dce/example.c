int bar() {
    return 5;
}

void f() {
}

void foo() {
    int x = bar();
    int y = x + 7;
    f();
}
