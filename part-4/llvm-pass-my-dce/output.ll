; ModuleID = 'example.ll'
source_filename = "example.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: nounwind uwtable
define i32 @bar() #0 {
  ret i32 5
}

; Function Attrs: nounwind uwtable
define void @f() #0 {
  ret void
}

; Function Attrs: nounwind uwtable
define void @foo() #0 {
  %1 = call i32 @bar()
  call void @f()
  ret void
}

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.0 (/afs/inf.ed.ac.uk/user/v/v1asmi18/ug3-compilers/llvm/tools/clang eec0daa08cb81e24034f2c74c936839867312db2) (/afs/inf.ed.ac.uk/user/v/v1asmi18/ug3-compilers/llvm d5fb62aebedf0926303bb7710f71018225ba0b05)"}
