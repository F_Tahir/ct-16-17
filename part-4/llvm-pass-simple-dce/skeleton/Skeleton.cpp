#define DEBUG_TYPE "opCounter"
#include "llvm/Pass.h"
#include "llvm/IR/Function.h"
#include "llvm/Support/raw_ostream.h"
#include <vector>
#include "llvm/ADT/SmallVector.h"
#include "llvm/Transforms/Utils/Local.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/Value.h"
#include "llvm/IR/User.h"
using namespace llvm;

// Used to store instructions that should be removed
SmallVector<Instruction*, 64> Worklist;

void countInstructions(Function &F) {

	std::map<std::string, int> opCounter;
	for (Function::iterator bb = F.begin(), e = F.end(); bb != e; ++bb) {
		for (BasicBlock::iterator i = bb->begin(), e = bb->end(); i != e; ++i) {
			if(opCounter.find(i->getOpcodeName()) == opCounter.end()) {
				opCounter[i->getOpcodeName()] = 1;
			} else {
				opCounter[i->getOpcodeName()] += 1;
			}
		}
	}

	std::map <std::string, int>::iterator i = opCounter.begin();
	std::map <std::string, int>::iterator e = opCounter.end();
	while (i != e) {
		errs() << i->first << ": " << i->second << "\n";
		i++;
	}
	opCounter.clear();
}


void eliminateDeadCode(Function &F) {

    // If an instruction is removed in a loop, it could cause other code
    // to be dead, in the next loop, so this flag allows us to loop again
    int deadCodeFlag = true;

    while (deadCodeFlag) {
        deadCodeFlag = false;

        // Iterate through each instruction in the function. Check if it's dead,
        // and if so, add it to Worklist to remove later.
        for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I) {
            Instruction *inst = &*I;

            // Check if instruction is dead, and add to Worklist if it is
            if (isInstructionTriviallyDead(inst, nullptr)) {
                Worklist.push_back(inst);
                deadCodeFlag = true; // An instruction was removed, so loop again
            }
        }

        // Now loop through Worklist and remove each instruction from code.
        for (Instruction *i : Worklist) {
           i->eraseFromParent();
        }
        Worklist.clear();
    }
}


namespace {
   struct SimpleDCE : public FunctionPass {
     static char ID;
     SimpleDCE() : FunctionPass(ID) {}


     virtual bool runOnFunction(Function &F) {
       errs() << "BEFORE DCE\n";
       countInstructions(F);
       errs() << "DCE START\n";
       eliminateDeadCode(F);
       countInstructions(F);
       errs() << "DCE END\n";
       return false;
     }
   };
}
char SimpleDCE::ID = 0;
static RegisterPass<SimpleDCE> X("simpledce", "Simple dead code elimination");
