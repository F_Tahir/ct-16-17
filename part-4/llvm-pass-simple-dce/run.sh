FILENAME="$1"
BASENAME="${FILENAME%%.*}"
LLFILE="${BASENAME}.ll"

rm build/
mkdir build/
cd build/
cmake ..
make
cd ..

echo $FILENAME
echo $BASENAME
echo $LLFILE

/home/faizan/Documents/Projects/ct-16-17/build/bin/clang -c -emit-llvm -S $FILENAME
/home/faizan/Documents/Projects/ct-16-17/build/bin/opt -load build/skeleton/libSkeletonPass.so -mem2reg -simpledce $LLFILE
