; ModuleID = 'example.c'
source_filename = "example.c"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

; Function Attrs: nounwind uwtable
define i32 @foo() #0 {
  %1 = alloca i32, align 4
  %2 = alloca i32, align 4
  %3 = alloca i32, align 4
  store i32 5, i32* %1, align 4
  %4 = load i32, i32* %1, align 4
  %5 = add nsw i32 %4, 7
  store i32 %5, i32* %2, align 4
  %6 = load i32, i32* %2, align 4
  %7 = sub nsw i32 %6, 8
  store i32 %7, i32* %3, align 4
  br label %8

; <label>:8:                                      ; preds = %11, %0
  %9 = load i32, i32* %1, align 4
  %10 = icmp slt i32 %9, 50
  br i1 %10, label %11, label %14

; <label>:11:                                     ; preds = %8
  %12 = load i32, i32* %1, align 4
  %13 = add nsw i32 %12, 1
  store i32 %13, i32* %1, align 4
  br label %8

; <label>:14:                                     ; preds = %8
  %15 = load i32, i32* %3, align 4
  ret i32 %15
}

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="true" "no-frame-pointer-elim-non-leaf" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.0 (/afs/inf.ed.ac.uk/user/v/v1asmi18/ug3-compilers/llvm/tools/clang eec0daa08cb81e24034f2c74c936839867312db2) (/afs/inf.ed.ac.uk/user/v/v1asmi18/ug3-compilers/llvm d5fb62aebedf0926303bb7710f71018225ba0b05)"}
