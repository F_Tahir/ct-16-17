"This is a \"string\" "
"This is a \n newline"
"This is a \t tabbed string"
"This is a \\ backslash"
"This is a \'c\'haracter"
"This is a normal string"
"This is \r carriage returned string"

// Now I'm testing some characters

'c'
'a'
'r'
'h'
'"' // This should work without an escape


/* Now I'll test some escaped characters *

'\''
'\"'
'\\'
'\n'
'\t'

/* These should be invalid 
This file should have 2 errors in total, because of the below tokens */

'\'
'long char'


// Another comment - everything below sohuld be fine

array
"String"
'c'


/*
a multiline
/*
comment */

char[10] c = "lol";





















































