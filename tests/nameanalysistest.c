// Struct declaration
struct Books {
    int bookId;
    int bookName;
};

// This should fail because a Books struct was already declared
struct Books {
   int bookId;
   int bookAuthor;
};

// This should fail because age was declared twice
struct Person {
    char name[50];
    int age;
    int age;
};


// Global variables
int i;
int seven;



void d() {
    struct Books book;

    // This should fail because Car hasn't been declared as struct
    struct Car car;
}


// This should fail because there is already an identifier i in the same scope (global variable)
void i() {
}


// This fails as it is local shadowing
void main(int j) {
    int j;
}


// This is global shadowing but should not fail.
void isPrime(int i) {
}


// This fails because a and b is called as a variable without first being declared
void register(int j) {
    j = 5;
    a = j + 1;
    b[10] = 5;
}


// This fails because o is returned but not declared
int number(int m) {
    int n;
    n = m + 5;
    return o;
}





// Testing built in functions

// Should not fail because print_c() is built in
void print() {
    print_c();
}


void read_i() {
    print("This should fail because read_i is already a built in function");
}




// Testing expressions

// This should fail because result was not declared
void add(int i) {
	result = i+5;
}

// This should pass successfully
void sub(int i) {
	int result;
	int a[5];
	
	result = a[1] - i;
}



// Should fail because digit declared twice, and seven not declared
int mult(int digit, int digit, int b) {

	seven = 7;
	add(seven);
	return 5;
	
	read_i();
}


int comparison(int int1) {
	if (int1 == int2) {
		return 1;
	}
}


int check(int a, int b) {
	while (a == b) {
		int a;
		a = 5;
		return 1;
	}
}































































































































